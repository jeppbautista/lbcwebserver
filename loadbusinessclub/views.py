from flask import Blueprint, jsonify, redirect, render_template, request, url_for, g, session
from flask_httpauth import HTTPBasicAuth

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import datetime
import decimal
import os
import pytz
import traceback
from werkzeug.utils import secure_filename

from loadbusinessclub import app, db, query
from loadbusinessclub.auth.views import token_required
from loadbusinessclub.helpers import decode_sponsor, encode_sponsor
from loadbusinessclub.models import *
from loadbusinessclub.models import _format_date

main = Blueprint("main", __name__)
auth = HTTPBasicAuth()


@main.route("/", strict_slashes=False)
@main.route("/login", strict_slashes=False)
@main.route("/register", strict_slashes=False)
@main.route("/app/main", strict_slashes=False)
@main.route("/app/admin/<page>", strict_slashes=False)
@main.route("/app/main/<page>", strict_slashes=False)
@main.route("/ecommerce/<page>", strict_slashes=False)
@main.route("/shop", strict_slashes=False)
@main.route("/shop/<page>", strict_slashes=False)
@main.route("/<page>", strict_slashes=False)
def index(page=""):
    return render_template("index.html")


@main.route("/api/test")
@token_required
def test():
    return jsonify(status="success")


@main.route("/api/activate", methods=["GET", "POST"])
@token_required
def activate():
    try:
        data = request.get_json()
        code = data["codeid"]
        userid = g.user.id

        user = User.query.filter(User.id == userid).first()
        user.activationid = code
        tz = pytz.timezone('Asia/Manila')
        user.activationdate = datetime.datetime.now(tz=tz)
        user.is_activated = 1
        db.session.commit()

        sql = query.unilevel_r().format(id=user.id)

        colnames = ["id", "username", "sponsorid"]

        results = db.session.execute(sql)
        result = [row for row in results]
        unilevel = [{colnames[col]: x for col, x in enumerate(r)} for r in result]

        for i in range(len(unilevel)):
            level = i
            unilevel[i]["level"] = level

        final = [uni for uni in unilevel[:8]]

        for fin in final[1:]:
            wallet = Wallet.query.filter(Wallet.user == fin["id"]).first()
            unilevel_earning = app.config["UNILEVEL_EARNING"][fin["level"]]
            wallet.unilevel_earning = decimal.Decimal(wallet.unilevel_earning) + decimal.Decimal(unilevel_earning)
            wallet.unilevel_collect = decimal.Decimal(wallet.unilevel_collect) + decimal.Decimal(unilevel_earning)

            db.session.add(
                TransactionUnilevel(
                    amount=decimal.Decimal(unilevel_earning),
                    userfrom=user.id,
                    userto=wallet.user,
                    level=fin["level"]
                )
            )

            db.session.commit()

        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/add-activation", methods=["GET", "POST"])
@token_required
def add_activation():
    try:
        data = request.get_json()
        code = data["code"]
        userid = g.user.id
        activation = Activation.query\
            .filter(Activation.activationcode == code)\
            .filter(Activation.user == userid)\
            .filter(Activation.status == "ACCEPTED")\
            .first()

        if not activation:
            return jsonify(status="Activation code does not exist"), 500
        else:
            activation.status = "ACTIVATED"
            db.session.commit()
            return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/activations", methods=["GET", "POST"])
@token_required
def activationcodes():

    def _activationcode(code):
        return {
            "id_activation": code.id,
            "created": code.created,
            "activationcode": code.activationcode,
            "status": code.status,
            "username": code.username,
            "user_activation": code.activationid
        }

    try:
        userid = g.user.id
        codes = Activation.query \
            .join(User, Activation.user == User.id) \
            .add_columns(
            Activation.id,
            Activation.created,
            Activation.activationcode,
            Activation.status,
            User.username,
            User.activationid
        ).filter(User.id == userid) \
            .filter((Activation.status == "ACCEPTED") | (Activation.status == "ACTIVATED")) \
            .filter(User.activationid != Activation.id) \
            .order_by(Activation.created.desc()).all()

        accode = Activation.query \
            .join(User, Activation.user == User.id) \
            .filter(User.id == userid) \
            .filter(User.activationid == Activation.id)\
            .filter((Activation.status == "ACCEPTED") | (Activation.status == "ACTIVATED")) \
            .first()

        pending = OrderDetail.query \
            .join(Orders, OrderDetail.order == Orders.id) \
            .join(UserCustomer, Orders.customer == UserCustomer.customer) \
            .filter(OrderDetail.product == 1) \
            .filter(UserCustomer.user == userid) \
            .all()

        data = {
            "my_activation": accode.activationcode if accode else None,
            "activations": [_activationcode(code) for code in codes] if len(codes) > 0 else [],
            "pending": len(pending)
        }

        return jsonify(data), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/activation-transfer", methods=["GET", "POST"])
@token_required
def activationtransfer():
    try:
        data = request.get_json()
        activationid = data["activationid"]

        userfrom = User.query.filter(User.username == data["usernamefrom"]).first()
        userto = User.query.filter(User.username == data["usernameto"]).first()

        activation = Activation.query\
            .filter(Activation.id == activationid)\
            .filter(Activation.user == userfrom.id).first()

        activation.user = userto.id

        db.session.add(
            TransactionActivation(
                activationid=activation.id,
                userfrom=userfrom.id,
                userto=userto.id
            )
        )

        # if not userto.is_activated:
        #     userto.is_activated = True
        #     userto.activationdate = datetime.datetime.now()
        #     userto.activationid = activationid
        db.session.commit()

        return jsonify(status="Successful"), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/referral", methods=["GET", "POST"])
@token_required
def referral_link():
    try:
        userid = g.user.id
        user = User.query.filter(User.id == userid).first()
        return jsonify(referral_link=user.username), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/singleline", methods=["GET", "POST"])
@token_required
def singleline():
    try:
        userid = g.user.id
        # singlelinedata = ViewSingleLine.query.filter_by(id=userid).first()
        sql = query.singleline().format(userid)

        colnames = ["id", "username", "created", "direct", "directactive",
                    "directinactive", "team", "teamactive",
                    "teaminactive", "level", "dailyreward",
                    "maxreward"]

        results = db.session.execute(sql)
        result = [row for row in results]
        singlelinedata = {colnames[col]: sing for col, sing in enumerate(result[0])}
        singlelinedata["dailyreward"] = float(singlelinedata["dailyreward"])
        # singlelinedata["maxreward"] = 0
        # TODO reward
        return jsonify(singlelinedata), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/wallet", methods=["GET", "POST"])
@token_required
def wallet():
    try:
        userid = g.user.id
        wallet = Wallet.query.filter(Wallet.user == userid).first()
        wallet.balance = wallet.singleline_earning \
            + wallet.unilevel_earning \
            + wallet.shop_earning
        return jsonify(wallet.tojson()), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/genealogy", methods=["GET", "POST"])
@token_required
def genealogy():
    try:
        userid = g.user.id
        username = g.user.username

        sql = query.genealogy().format(username=username, id=userid)

        colnames = ["main", "lvl1", "lvl1_sponsor", "lvl1_created",
                    "lvl2", "lvl2_sponsor", "lvl2_created",
                    "lvl3", "lvl3_sponsor", "lvl3_created",
                    "lvl4", "lvl4_sponsor", "lvl4_created",
                    "lvl5", "lvl5_sponsor", "lvl5_created",
                    "lvl6", "lvl6_sponsor", "lvl6_created",
                    "lvl7", "lvl7_sponsor", "lvl7_created", ]

        results = db.session.execute(sql)
        result = [row for row in results]
        unilevel = [{colnames[col]: _format_date(x) if "created" in colnames[col] else x for col, x in enumerate(r)} for r in result]
        return jsonify(unilevel)
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/unilevel", methods=["GET", "POST"])
@token_required
def unilevel():
    try:
        userid = g.user.id
        username = g.user.username

        sql = query.unilevel().format(username=username, id=userid)

        results = db.session.execute(sql)
        result = [row for row in results]
        return jsonify(unilevel_team=len(result))
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/unilevel-ecommerce", methods=["GET", "POST"])
@token_required
def unilevel_ecomm():
    try:
        userid = g.user.id

        tcommerce = TransactionEcommerce.query\
            .filter(TransactionEcommerce.user == userid)\
            .filter(TransactionEcommerce.earningtype == "UNILEVEL")\
            .all()
        return jsonify(income=sum([float(t.amount) for t in tcommerce]))

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/top-activations")
def top_activations():
    def _user_detail(userdetail):
        return {
            "sponsorid": userdetail.sponsorid,
            "is_activated": userdetail.is_activated,
            "activationdate": _format_date(userdetail.activationdate),
            "activationid": userdetail.activationid,
            "name": userdetail.name,
            "username": userdetail.username,
            "created": userdetail.created,
            "phone": userdetail.phone,
            "email": userdetail.email,
            "country": userdetail.country,
            "province": userdetail.province,
            "city": userdetail.city
        }
    try:
        users = User.query \
            .join(UserDetails, User.id == UserDetails.user) \
            .add_columns(
                User.username,
                User.created,
                User.sponsorid,
                User.is_activated,
                User.activationid,
                User.activationdate,
                User.is_admin,
                UserDetails.name,
                UserDetails.phone,
                UserDetails.email,
                UserDetails.country,
                UserDetails.province,
                UserDetails.city
            )\
            .filter(User.activationdate != None) \
            .order_by(User.activationdate.desc()) \
            .limit(10).all()
        return jsonify([_user_detail(user) for user in users])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/upload-picture", methods=["GET", "POST"])
def upload_picture():
    try:
        if "file" not in request.files:
            return jsonify(status="No file part"), 500

        f = request.files["file"]
        username = request.form.get("username", None)

        if not username:
            return jsonify(status="Invalid"), 500

        if f.filename == "":
            return jsonify(status="No file selected"), 500

        user = User.query.filter(User.username == username).first()

        if f:
            extension = f.filename.split(".")[-1]
            filename = "avatar-{userid}.{extension}" \
                .format(userid=user.id, extension=extension)

            f.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            user_detail = UserDetails.query \
                .filter(UserDetails.user == user.id).first()
            user_detail.avatar = filename
            db.session.commit()
            app.logger.error(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            return jsonify(status="Success", filepath=os.path.join(app.config["UPLOAD_FOLDER"], filename)), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/upload-receipt", methods=["GET", "POST"])
def upload():
    try:
        if "file" not in request.files:
            return jsonify(status="No file part"), 500

        f = request.files["file"]
        orderid = request.form.get("orderid", 0)

        if f.filename == "":
            return jsonify(status="No file selected"), 500

        if f:
            extension = f.filename.split(".")[-1]
            filename = "receipt-{orderid}.{extension}" \
                .format(orderid=orderid, extension=extension)

            f.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            order = Orders.query \
                .filter(Orders.id == orderid).first()
            order.receipt = filename
            db.session.commit()
            app.logger.error(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            return jsonify(status="Success", filepath=os.path.join(app.config["UPLOAD_FOLDER"], filename)), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/upload-receipt-activation", methods=["GET", "POST"])
def upload_activation():
    try:
        if "file" not in request.files:
            app.logger.error("No file part")
            return jsonify(status="No file part"), 500

        f = request.files["file"]
        orderid = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")

        if f.filename == "":
            app.logger.error("No file selected")
            return jsonify(status="No file selected"), 500

        if f:
            extension = f.filename.split(".")[-1]
            filename = "receipt-{orderid}.{extension}" \
                .format(orderid=orderid, extension=extension)

            f.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))

            app.logger.error(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            return jsonify(status="Success",
                           filepath=os.path.join(app.config["UPLOAD_FOLDER"], filename),
                           filename=filename), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/user", methods=["GET", "POST"])
@token_required
def userdata():
    try:
        userid = g.user.id
        user = User.query.filter(User.id == userid).first()
        return jsonify(user.tojson()), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/sponsor", methods=["GET", "POST"])
@token_required
def sponsordata():
    try:
        userid = g.user.id
        user = User.query.filter(User.id == userid).first()
        sponsor = User.query.filter(User.id == user.sponsorid).first()
        return jsonify(sponsor.tojson()), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/user-detail", methods=["GET", "POST"])
@token_required
def user_detail():
    def _user_detail(userdetail):
        return {
            "sponsorid": userdetail.sponsorid,
            "is_activated": userdetail.is_activated,
            "is_admin": userdetail.is_admin,
            "activationid": userdetail.activationid,
            "username": userdetail.username,
            "name": userdetail.name,
            "phone": userdetail.phone,
            "email": userdetail.email,
            "country": userdetail.country,
            "province": userdetail.province,
            "city": userdetail.city,
            "birthday": _format_date(userdetail.birthday, "%Y-%m-%d"),
            "avatar": userdetail.avatar
        }

    try:
        userid = g.user.id
        user = User.query \
            .join(UserDetails, User.id == UserDetails.user) \
            .add_columns(
                User.sponsorid,
                User.is_activated,
                User.activationid,
                User.activationdate,
                User.is_admin,
                User.username,
                UserDetails.name,
                UserDetails.phone,
                UserDetails.email,
                UserDetails.country,
                UserDetails.province,
                UserDetails.city,
                UserDetails.birthday,
            UserDetails.avatar
        ).filter(User.id == userid).first()
        print(_user_detail(user))
        return jsonify(_user_detail(user)), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/user-settings", methods=["GET", "POST"])
@token_required
def user_settings():
    try:
        userid = g.user.id
        data = request.get_json()

        if data.get("province", "PHL") == "PHL" \
                or data.get("city", "PHL") == "PHL":

            return jsonify(status="Some fields missing"), 500

        user = User.query.filter(User.id == userid).first()

        if not user.validate_password(data.get("password")):
            app.logger.error("Invalid password")
            return jsonify(status="Invalid password"), 500

        user_detail = UserDetails.query.filter(UserDetails.user == user.id).first()

        db.session.add(
            TransactionUpdate(
                user=userid,
                old_name=user_detail.name,
                old_email=user_detail.email,
                old_country=user_detail.country,
                old_province=user_detail.province,
                old_city=user_detail.city,
                old_phone=user_detail.phone
            )
        )

        user_detail.name = data.get("name", user_detail.name)
        user_detail.email = data.get("email", user_detail.email)
        user_detail.country = data.get("country", user_detail.country)
        user_detail.province = data.get("province", user_detail.province)
        user_detail.city = data.get("city", user_detail.city)
        user_detail.phone = data.get("phone", user_detail.phone)
        user_detail.birthday = data.get("birthday", user_detail.birthday)

        db.session.commit()
        app.logger.error("DONE")
        return jsonify(status="Success"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@main.route("/api/reset-password", methods=["GET", "POST"])
def reset_password():
    try:
        data = request.get_json()
        username = data.get("username")
        email = data.get("email")

        user = User.query\
            .filter(User.username == username).first()

        user_detail = UserDetails.query\
            .filter(UserDetails.email == email).first()

        if user.id != user_detail.user:
            app.logger.error("Email and/or username does not match")
            return jsonify(status="Something went wrong"), 500

        newpass = datetime.datetime.now().strftime("%d%M%S%f")

        user.password = newpass
        db.session.commit()

        data = request.get_json()
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "New password for {}".format(username)
        msg['From'] = app.config["EMAIL_AUTO"]
        msg['To'] = email

        body = "Your new password is: {}".format(newpass)

        mime = MIMEText(body, "html")
        msg.attach(mime)

        s = smtplib.SMTP_SSL(host="mail.loadbusinessclub.com", port=465)
        s.login(app.config["EMAIL_AUTO"], app.config["EMAIL_PASS"])
        s.sendmail(app.config["EMAIL_AUTO"],
                   app.config["EMAIL_ADMIN"],
                   msg.as_string())
        s.quit()

        print(newpass)

        app.logger.info("Message sent")
        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500