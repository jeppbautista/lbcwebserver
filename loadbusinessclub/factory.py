import datetime
import decimal

from loadbusinessclub import db
from loadbusinessclub.models import *


def init_admin():
    db.session.add(User(username="admin", password="pass1234"))
    db.session.commit()


def init_product():
    db.session.add(Product(
        merchant=1,
        productname="Activation code",
        unitprice=250,
    ))
    db.session.commit()


def init_payment():
    db.session.add(Payment(paymenttype="COD"))
    db.session.add(Payment(paymenttype="PAYMAYA"))
    db.session.add(Payment(paymenttype="GCASH"))
    db.session.add(Payment(paymenttype="COINSPH"))
    db.session.add(Payment(paymenttype="BDO"))
    db.session.add(Payment(paymenttype="BPI"))
    db.session.add(Payment(paymenttype="WALLET"))
    db.session.commit()


def init_usertype():
    db.session.add(UserType(usertype="DEALER"))
    db.session.add(UserType(usertype="MOBILE"))
    db.session.add(UserType(usertype="CITY"))
    db.session.add(UserType(usertype="PROVINCIAL"))
    db.session.commit()


def retro_unilevel():
    def _activate(data):
        if 1 == 1:
            code = data["codeid"]
            userid = data["id"]

            user = User.query.filter(User.id == userid).first()
            user.activationid = code
            user.activationdate = datetime.datetime.now()
            user.is_activated = 1
            db.session.commit()

            sql = """
            SELECT *
            FROM (
            select  id,
                    username,
                    sponsorid
            from    (select * from user
                    WHERE is_activated = 1
                     order by created DESC) products_sorted,
                    (select @pv := '{id}') initialisation
            where   find_in_set(id, @pv)
            and     length(@pv := concat(@pv, ',', sponsorid))
            ) x
            """.format(id=user.id)

            colnames = ["id", "username", "sponsorid"]

            results = db.session.execute(sql)
            result = [row for row in results]
            unilevel = [{colnames[col]: x for col, x in enumerate(r)} for r in result]

            for i in range(len(unilevel)):
                level = i
                unilevel[i]["level"] = level

            final = [uni for uni in unilevel[:7]]

            for fin in final[1:]:
                wallet = Wallet.query.filter(Wallet.user == fin["id"]).first()
                unilevel_earning = app.config["UNILEVEL_EARNING"][fin["level"]]
                wallet.unilevel_earning = wallet.unilevel_earning + decimal.Decimal(unilevel_earning)
                db.session.commit()

            return "SUCCESS"

    users = User.query.all()
    for user in users:
        print(_activate({"id": user.id, "codeid": 1}))


def retro_singleline(days=3):
    users = User.query.all()
    for user in users:
        sql = """ 
                SELECT *,
                  CASE
                    WHEN a.level = 1 THEN 15
                    WHEN a.level = 2 THEN 17.50
                    WHEN a.level = 3 THEN 20
                    WHEN a.level = 4 THEN 30
                    WHEN a.level = 5 THEN 45
                    WHEN a.level = 6 THEN 62.50
                    WHEN a.level = 7 THEN 107.50
                    WHEN a.level = 8 THEN 157.50
                    WHEN a.level = 9 THEN 300
                    WHEN a.level = 10 THEN 450
                    WHEN a.level = 11 THEN 625
                    WHEN a.level = 12 THEN 825
                    WHEN a.level = 13 THEN 1050
                    WHEN a.level = 14 THEN 1300
                    WHEN a.level = 15 THEN 1575
                    WHEN a.level = 16 THEN 1875
                    WHEN a.level = 17 THEN 2000
                    WHEN a.level = 18 THEN 2250
                    WHEN a.level = 19 THEN 2500
                    WHEN a.level = 20 THEN 3000
                    ELSE 0
                  END AS dailyreward,
                  CASE
                    WHEN a.level = 1 THEN 150
                    WHEN a.level = 2 THEN 350
                    WHEN a.level = 3 THEN 600
                    WHEN a.level = 4 THEN 1200
                    WHEN a.level = 5 THEN 2250
                    WHEN a.level = 6 THEN 3750
                    WHEN a.level = 7 THEN 7500
                    WHEN a.level = 8 THEN 15000
                    WHEN a.level = 9 THEN 30000
                    WHEN a.level = 10 THEN 54000
                    WHEN a.level = 11 THEN 93750
                    WHEN a.level = 12 THEN 148500
                    WHEN a.level = 13 THEN 220000
                    WHEN a.level = 14 THEN 325000
                    WHEN a.level = 15 THEN 465750
                    WHEN a.level = 16 THEN 637000
                    WHEN a.level = 17 THEN 850000
                    WHEN a.level = 18 THEN 1100000
                    WHEN a.level = 19 THEN 1400000
                    WHEN a.level = 20 THEN 1625000
                    ELSE 0
                  END AS maxreward
                FROM (
                  SELECT  *, 
                    CASE
                      WHEN main.directactive >= 210 AND main.team >= 1000000 THEN 20
                      WHEN main.directactive >= 190 AND main.team >= 715000 THEN 19
                      WHEN main.directactive >= 171 AND main.team >= 515000 THEN 18
                      WHEN main.directactive >= 153 AND main.team >= 365000 THEN 17
                      WHEN main.directactive >= 136 AND main.team >= 255000 THEN 16
                      WHEN main.directactive >= 120 AND main.team >= 175000 THEN 15
                      WHEN main.directactive >= 105 AND main.team >= 115000 THEN 14
                      WHEN main.directactive >= 91 AND main.team >= 70000 THEN 13
                      WHEN main.directactive >= 78 AND main.team >= 40000 THEN 12
                      WHEN main.directactive >= 66 AND main.team >= 20000 THEN 11
                      WHEN main.directactive >= 55 AND main.team >= 10000 THEN 10
                      WHEN main.directactive >= 45 AND main.team >= 5000 THEN 9
                      WHEN main.directactive >= 36 AND main.team >= 2500 THEN 8
                      WHEN main.directactive >= 28 AND main.team >= 1000 THEN 7
                      WHEN main.directactive >= 21 AND main.team >= 500 THEN 6
                      WHEN main.directactive >= 15 AND main.team >= 200 THEN 5
                      WHEN main.directactive >= 10 AND main.team >= 100 THEN 4
                      WHEN main.directactive >= 6 AND main.team >= 50 THEN 3
                      WHEN main.directactive >= 3 AND main.team >= 25 THEN 2
                      WHEN main.directactive >= 1 AND main.team >= 10 THEN 1
                      ELSE 0
                    END AS level
                  FROM (
                    SELECT id, username, created,
                      (SELECT COUNT(1) FROM user WHERE x.id=sponsorid) as direct, 
                      (SELECT COUNT(1) FROM user WHERE x.id=sponsorid AND is_activated=1) as directactive,
                      (SELECT COUNT(1) FROM user WHERE x.id=sponsorid AND is_activated=0) as directinactive,
                      (SELECT COUNT(1) FROM user WHERE x.created < created) as team,
                      (SELECT COUNT(1) FROM user b WHERE x.created < b.created AND (SELECT COUNT(1) FROM user WHERE b.id=sponsorid) >= 1 ) as teamactive,
                      (SELECT COUNT(1) FROM user b WHERE x.created < b.created AND (SELECT COUNT(1) FROM user WHERE b.id=sponsorid) < 1 ) as teaminactive
                    FROM user x
                    WHERE id={}
                  ) as main
                ) as a
                """.format(user.id)
        colnames = ["id", "username", "created", "direct", "directactive",
                    "directinactive", "team", "teamactive",
                    "teaminactive", "level", "dailyreward",
                    "maxreward"]

        results = db.session.execute(sql)
        result = [row for row in results]
        singlelinedata = {colnames[col]: sing for col, sing in enumerate(result[0])}

        wallet = Wallet.query\
            .filter(Wallet.user == user.id)\
            .first()

        wallet.singleline_earning = singlelinedata["dailyreward"] * days
        db.session.commit()
