from flask_login import UserMixin
from passlib.hash import pbkdf2_sha256 as hasher
from sqlalchemy.sql import func

from loadbusinessclub import app, db, login_manager

from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)


class User(UserMixin, db.Model):

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    usertypeid = db.Column(db.Integer, db.ForeignKey("user_type.id", ondelete="CASCADE"))

    username = db.Column(db.String(30), unique=True, nullable=False)
    passwordhash = db.Column(db.String(256), nullable=False)
    sponsorid = db.Column(db.Integer, default=1)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    activationid = db.Column(db.Integer, default=0)
    activationdate = db.Column(db.TIMESTAMP(timezone=True), default=None)
    is_activated = db.Column(db.Boolean(), default=False)
    is_admin = db.Column(db.Boolean(), default=False)
    product_quota = db.Column(db.Integer, default=3)
    shippingmm = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    shippingnonmm = db.Column(db.DECIMAL(precision=30, scale=10), default=0)

    def __repr__(self):
        return "<User: {}>".format(self.username)

    @property
    def password(self):
        raise AttributeError("password not readable")

    @password.setter
    def password(self, password):
        self.passwordhash = hasher.encrypt(password, rounds=2000, salt=str.encode(app.secret_key))

    def validate_password(self, password):
        return hasher.verify(password, self.passwordhash)

    def is_active(self):
        return True

    def is_authenticated(self):
        return False

    def get_id(self):
        return self.id

    def generate_auth_token(self, expiration=1800):
        s = Serializer(app.config["SECRET_KEY"])
        return s.dumps({"id": self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config["SECRET_KEY"])
        try:
            data = s.loads(token)
        except SignatureExpired:
            app.logger.error("Expired Signature")
            return None
        except BadSignature:
            app.logger.error("Bad Signature")
            return None
        user = User.query.get(data["id"])
        return user

    def tojson(self):
        return {
            "id": self.id,
            "usertypeid": self.usertypeid,
            "username": self.username,
            "sponsorid": self.sponsorid,
            "created": _format_date(self.created),
            "activationid": self.activationid,
            "activationdate": _format_date(self.activationdate),
            "is_activated": self.is_activated,
            "product_quota": self.product_quota,
            "shippingmm": float(self.shippingmm),
            "shippingnonmm": float(self.shippingnonmm)
        }


class UserDetails(db.Model):

    __tablename__ = "user_details"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))

    name = db.Column(db.String(50))
    phone = db.Column(db.String(30))
    email = db.Column(db.String(50), nullable=False)
    birthday = db.Column(db.Date, default=None)
    country = db.Column(db.String(50))
    province = db.Column(db.String(50))
    city = db.Column(db.String(50))
    avatar = db.Column(db.String(255), default=None)

    def tojson(self):
        return {
            "id": self.id,
            "user": self.user,
            "name": self.name,
            "phone": self.phone,
            "email": self.email,
            "country": self.country,
            "province": self.province,
            "city": self.city,
            "birthday": _format_date(self.birthday, "%Y-%m-%d"),
            "avatar": self.avatar
        }


class Activation(db.Model):

    __tablename__ = "activation"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    order = db.Column(db.Integer, db.ForeignKey("orders.id"))
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    activationcode = db.Column(db.String(50), nullable=False)
    status = db.Column(db.String(50), default="PENDING")  # [PENDING, APPROVED, REGISTERED]

    def tojson(self):
        return {
            "id": self.id,
            "user": self.user,
            "created": self.created,
            "activioncode": self.activationcode,
            "status": self.status,
            "order": self.order
        }


class UserType(db.Model):

    __tablename__ = "user_type"

    id = db.Column(db.Integer, primary_key=True)
    usertype = db.Column(db.String(50))  # [INACTIVE, DEALER, MOBILE, CITY, PROVINCIAL]


class Wallet(db.Model):

    __tablename__ = "wallet"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer)
    balance = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    withdrawn = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    singleline_earning = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    singleline_collect = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    unilevel_earning = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    unilevel_collect = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    shop_earning = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    team_earning = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    ecomm_earning = db.Column(db.DECIMAL(precision=30, scale=10), default=0)

    def tojson(self):
        return {
            "id": self.id,
            "user": self.user,
            "balance": float(self.balance),
            "singleline_earning": float(self.singleline_earning),
            "singleline_collect": float(self.singleline_collect),
            "unilevel_earning": float(self.unilevel_earning),
            "unilevel_collect": float(self.unilevel_collect),
            "shop_earning": float(self.shop_earning),
            "team_earning": float(self.team_earning),
            "ecomm_earning": float(self.ecomm_earning),
            "withdrawn": float(self.withdrawn)
        }


class Product(db.Model):

    __tablename__ = "product"

    id = db.Column(db.Integer, primary_key=True)
    merchant = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    category = db.Column(db.String(128), default="OTHERS")
    productname = db.Column(db.String(128))
    productdescription = db.Column(db.Text)
    unitprice = db.Column(db.DECIMAL(precision=30, scale=10))
    discountprice = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    shippingmm = db.Column(db.DECIMAL(precision=30, scale=10))
    shippingnonmm = db.Column(db.DECIMAL(precision=30, scale=10))
    image = db.Column(db.String(256), default="default.jpg")
    image2 = db.Column(db.String(256))
    image3 = db.Column(db.String(256))
    image4 = db.Column(db.String(256))
    image5 = db.Column(db.String(256))
    ranking = db.Column(db.Integer, default=0)
    is_available = db.Column(db.Boolean(), default=False)
    is_deleted = db.Column(db.Boolean(), default=False)

    def tojson(self):
        return {
            "id": self.id,
            "merchant": self.merchant,
            "category": self.category,
            "productname": self.productname,
            "productdescription": self.productdescription,
            "unitprice": float(self.unitprice),
            "discountprice": float(self.discountprice),
            "shippingmm": float(self.shippingmm),
            "shippingnonmm": float(self.shippingnonmm),
            "image": self.image,
            "image2": self.image2,
            "image3": self.image3,
            "image4": self.image4,
            "image5": self.image5,
            "ranking": self.ranking,
            "is_deleted": self.is_deleted,
            "is_available": self.is_available
        }


class ProductCategory(db.Model):

    __tablename__ = "product_category"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))


class Customer(db.Model):

    __tablename__ = "customer"

    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(128))
    address = db.Column(db.String(256))
    province = db.Column(db.String(50))
    city = db.Column(db.String(50))
    phone = db.Column(db.String(30))
    email = db.Column(db.String(50))

    def tojson(self):
        return {
            "id": self.id,
            "fullname": self.fullname,
            "address": self.address,
            "city": self.city,
            "province": self.province,
            "phone": self.phone,
            "email": self.email
        }


class UserCustomer(db.Model):
    __tablename__ = "user_customer"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    customer = db.Column(db.Integer, db.ForeignKey("customer.id", ondelete="CASCADE"))


class Payment(db.Model):

    __tablename__ = "payment"

    id = db.Column(db.Integer, primary_key=True)
    paymenttype = db.Column(db.String(50))

    def tojson(self):
        return {
            "id": self.id,
            "paymenttype": self.paymenttype
        }


class Orders(db.Model):

    __tablename__ = "orders"

    id = db.Column(db.Integer, primary_key=True)
    customer = db.Column(db.Integer, db.ForeignKey("customer.id", ondelete="CASCADE"))
    payment = db.Column(db.Integer, db.ForeignKey("payment.id", ondelete="CASCADE"))

    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    ordernumber = db.Column(db.String(50), unique=True)  # customer+payment+month+day+id
    shipdate = db.Column(db.TIMESTAMP(timezone=True), default=None)
    requireddate = db.Column(db.TIMESTAMP(timezone=True), default=None)
    status = db.Column(db.String(50))  # ACCEPTED, DENIED, PENDING, SHIPPING, DELIVERING, CANCELLED, "RECEIVED"
    err = db.Column(db.String(256))
    paymentdate = db.Column(db.TIMESTAMP(timezone=True), default=None)
    receipt = db.Column(db.String(256), default="default.jpg")
    grand_total = db.Column(db.DECIMAL(precision=30, scale=10), default=0)

    is_deleted = db.Column(db.Boolean(), default=False)
    is_paid = db.Column(db.Boolean(), default=False)

    def tojson(self):
        return {
            "id": self.id,
            "customer": self.customer,
            "payment": self.payment,
            "created": _format_date(self.created),
            "ordernumber": self.ordernumber,
            "shipdate": self.shipdate,
            "requireddate": self.requireddate,
            "status": self.status,
            "err": self.err,
            "paymentdate": self.paymentdate,
            "receipt": self.receipt,
            "is_deleted": self.is_deleted,
            "is_paid": self.is_paid,
            "grand_total": float(self.grand_total)
        }


class OrderDetail(db.Model):

    __tablename__ = "order_detail"

    id = db.Column(db.Integer, primary_key=True)
    product = db.Column(db.Integer, db.ForeignKey("product.id", ondelete="CASCADE"))
    order = db.Column(db.Integer, db.ForeignKey("orders.id", ondelete="CASCADE"))
    unitprice = db.Column(db.DECIMAL(precision=30, scale=10))
    qty = db.Column(db.Integer, default=1)
    subtotal = db.Column(db.DECIMAL(precision=30, scale=10))
    discount = db.Column(db.DECIMAL(precision=30, scale=10))
    lbctax = db.Column(db.DECIMAL(precision=30, scale=10))
    total = db.Column(db.DECIMAL(precision=30, scale=10))
    shippingfee = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    status = db.Column(db.String(50), default="PENDING")

    def tojson(self):
        return {
            "id": self.id,
            "product": self.product,
            "order": self.order,
            "unitprice": float(self.unitprice),
            "qty": self.qty,
            "subtotal": float(self.subtotal),
            "discount": float(self.discount),
            "lbctax": float(self.lbctax),
            "total": float(self.total),
            "shippingfee": float(self.shippingfee),
            "status": self.status
        }


class Transaction(db.Model):

    __tablename__ = "transaction"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    transactiontype = db.Column(db.String(50))  # TRANSFER, PURCHASE, REWARD, UPLOAD


class TransactionActivation(db.Model):

    __tablename__ = "transaction_activation"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    activationid = db.Column(db.Integer, db.ForeignKey("activation.id", ondelete="CASCADE"))
    userfrom = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    userto = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))


class TransactionDaily(db.Model):

    __tablename__ = "transaction_daily"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey("user.id"))
    username = db.Column(db.String(30))
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    message = db.Column(db.String(512))
    status = db.Column(db.String(50))  # ACCEPTED, DENIED
    old_balance = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    new_balance = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    old_collect = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    new_collect = db.Column(db.DECIMAL(precision=30, scale=10), default=0)

    def tojson(self):
        return {
            "id": self.id,
            "username": self.username,
            "created": _format_date(self.created),
            "message": self.message,
            "status": self.status,
            "old_balance": float(self.old_balance),
            "new_balance": float(self.new_balance),
            "old_collect": float(self.old_collect),
            "new_collect": float(self.new_collect)
        }


class TransactionEcommerce(db.Model):

    __tablename__ = "transaction_ecommerce"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    user = db.Column(db.Integer, db.ForeignKey("user.id"))
    order_detail = db.Column(db.Integer, db.ForeignKey("order_detail.id"))
    amount = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    earningtype = db.Column(db.String(30))
    earningfrom = db.Column(db.Integer, db.ForeignKey("user.id"))

    def tojson(self):
        return {
            "id": self.id,
            "created": _format_date(self.created),
            "user": self.user,
            "order_detail": self.order_detail,
            "amount": float(self.amount),
            "earningtype": self.earningtype,
            "earningfrom": self.earningfrom
        }


class TransactionQuota(db.Model):

    __tablename__ = "transaction_quota"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    user = db.Column(db.Integer, db.ForeignKey("user.id"))
    count = db.Column(db.Integer)
    amount = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    status = db.Column(db.String(30), default="PENDING")
    receipt = db.Column(db.String(256), default="default.jpg")

    def tojson(self):
        return {
            "id": self.id,
            "created": _format_date(self.created),
            "user": self.user,
            "count": self.count,
            "amount": float(self.amount),
            "status": self.status,
            "receipt": self.receipt
        }


class TransactionTransfer(db.Model):

    __tablename__ = "transaction_transfer"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    amount = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    walletfrom = db.Column(db.Integer)
    walletto = db.Column(db.Integer)
    subwallet = db.Column(db.String(30))

    def tojson(self):
        return {
            "id": self.id,
            "created":  _format_date(self.created),
            "amount": float(self.amount),
            "walletfrom": self.walletfrom,
            "walletto": self.walletto,
            "subwallet": self.subwallet
        }


class TransactionUnilevel(db.Model):

    __tablename__ = "transaction_unilevel"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    amount = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    userfrom = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    userto = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    level = db.Column(db.Integer)

    def tojson(self):
        return {
            "id": self.id,
            "created": _format_date(self.created),
            "amount": float(self.amount),
            "userfrom": self.userfrom,
            "userto": self.userto,
            "level": self.level
        }


class TransactionUpdate(db.Model):

    __tablename__ = "transaction_update"

    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey("user.id", ondelete="CASCADE"))
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    old_name = db.Column(db.String(50))
    old_phone = db.Column(db.String(30))
    old_email = db.Column(db.String(50), nullable=False)
    old_country = db.Column(db.String(50))
    old_province = db.Column(db.String(50))
    old_city = db.Column(db.String(50))


class TransactionWithdraw(db.Model):

    __tablename__ = "transaction_withdraw"

    id = db.Column(db.Integer, primary_key=True)
    requestedby = db.Column(db.Integer, db.ForeignKey("user.id"))
    username = db.Column(db.String(30))
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    amount = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    balance = db.Column(db.DECIMAL(precision=30, scale=10), default=0)
    paytype = db.Column(db.String(30))
    accountnumber = db.Column(db.String(50))
    accountname = db.Column(db.String(50))
    wallet = db.Column(db.Integer)
    wallettype = db.Column(db.String(30))
    status = db.Column(db.String(30))

    def tojson(self):
        return {
            "id": self.id,
            "username": self.username,
            "created": _format_date(self.created),
            "amount": float(self.amount),
            "balance": float(self.balance),
            "requestedby": self.requestedby,
            "paytype": self.paytype,
            "accountnumber": self.accountnumber,
            "accountname": self.accountname,
            "wallet": self.wallet,
            "wallettype": self.wallettype,
            "status": self.status
        }


class ViewSingleLine(db.Model):

    __tablename__ = "vw_singleline"
    # __table_args__ = {'info': dict(is_view=True)}

    id = db.Column(db.Integer, primary_key=True)
    direct = db.Column(db.Integer)
    directactive = db.Column(db.Integer)
    directinactive = db.Column(db.Integer)
    team = db.Column(db.Integer)
    teamactive = db.Column(db.Integer)
    teaminactive = db.Column(db.Integer)
    level = db.Column(db.Integer)
    dailyreward = db.Column(db.Integer)
    maxreward = db.Column(db.Integer)
    username = db.Column(db.String(30), unique=True, nullable=False)
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())

    def tojson(self):
        return {
            "id": self.id,
            "username": self.username,
            "created": _format_date(self.created),
            "direct": self.direct,
            "directactive": self.directactive,
            "directinactive": self.directinactive,
            "team": self.team,
            "teamactive": self.teamactive,
            "teaminactive": self.teaminactive,
            "level": self.level,
            "dailyreward": float(self.dailyreward),
            "maxreward": self.maxreward
        }


class ViewUser(db.Model):

    __tablename__ = "vw_user"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30))
    sponsor = db.Column(db.String(30))
    name = db.Column(db.String(50))
    phone = db.Column(db.String(30))
    email = db.Column(db.String(50))
    status = db.Column(db.String(30))
    created = db.Column(db.TIMESTAMP(timezone=True), server_default=func.current_timestamp())
    usertype = db.Column(db.String(30))

    def tojson(self):
        return {
            "id": self.id,
            "username": self.username,
            "sponsor": self.sponsor,
            "name": self.name,
            "phone": self.phone,
            "email": self.email,
            "status": self.status,
            "created": _format_date(self.created),
            "usertype": self.usertype
        }

def _format_date(dt, format="%Y-%m-%d %H:%M:%S"):
    return dt.strftime(format) if dt else None
