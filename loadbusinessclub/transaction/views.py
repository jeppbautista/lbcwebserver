from flask import Blueprint, jsonify, redirect, render_template, request, url_for, g, session
from flask_httpauth import HTTPBasicAuth
import traceback

from sqlalchemy.orm import aliased

from loadbusinessclub import app, db, query
from loadbusinessclub.auth.views import token_required
from loadbusinessclub.models import *

tx = Blueprint("tx", __name__)
auth = HTTPBasicAuth()


@tx.route("/api/tx/daily", methods=["GET", "POST"])
@token_required
def tx_daily():
    try:
        userid = g.user.id
        tx = TransactionDaily.query.filter(TransactionDaily.user == userid).all()
        return jsonify([t.tojson() for t in tx] if len(tx) > 0 else [])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@tx.route("/api/tx/admin/daily", methods=["GET", "POST"])
@token_required
def tx_admin_daily():
    try:
        tx = TransactionDaily.query.order_by(TransactionDaily.created.desc()).all()
        return jsonify([t.tojson() for t in tx] if len(tx) > 0 else [])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@tx.route("/api/tx/admin/transfer", methods=["GET", "POST"])
@token_required
def tx_admin_transfer():
    def _tx(trans):
        return {
            "transaction_transfer": trans[0].tojson(),
            "walletfrom": trans[1].tojson(),
            "userfrom": trans[2].tojson(),
            "walletto": trans[3].tojson(),
            "userto": trans[4].tojson()
        }
    try:
        walletfrom = aliased(Wallet)
        walletto = aliased(Wallet)
        userfrom = aliased(User)
        userto = aliased(User)

        tx = db.session.query(TransactionTransfer, walletfrom,  userfrom, walletto, userto)\
            .join(walletfrom, TransactionTransfer.walletfrom == walletfrom.id) \
            .join(userfrom, walletfrom.user == userfrom.id) \
            .join(walletto, TransactionTransfer.walletto == walletto.id) \
            .join(userto, walletto.user == userto.id) \
            .order_by(TransactionTransfer.created.desc())\
            .all()

        return jsonify([_tx(t) for t in tx])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@tx.route("/api/tx/admin/withdraw", methods=["GET", "POST"])
@token_required
def tx_admin_withdraw():
    try:
        tx = TransactionWithdraw.query.order_by(TransactionWithdraw.created.desc()).all()
        return jsonify([t.tojson() for t in tx] if len(tx) > 0 else [])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500
