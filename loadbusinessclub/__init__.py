from flask import Flask, redirect, render_template, request, g
from flask_cors import CORS
from flask_httpauth import HTTPBasicAuth
from flask_jwt_extended import JWTManager
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

import datetime
import logging
import os

import wtforms_json
wtforms_json.init()


def setup_logging():
    format_str = "[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(message)s"
    file_handler = logging.FileHandler("logs/{:%Y-%m-%d}.log".format(datetime.datetime.now()))
    formatter = logging.Formatter(format_str)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)

    app.logger.addHandler(file_handler)
    app.logger.addHandler(stream_handler)


web_app = os.path.abspath(os.path.join(os.getcwd(), "../", "web-app/"))

app = Flask(__name__,
            static_folder=os.path.join(web_app, "build/tables"),
            template_folder=os.path.join(web_app, "build"))
# Talisman(app)
CORS(app, expose_headers="Authorization")
basicauth = HTTPBasicAuth()

os.environ["TZ"] = "Asia/Manila"

app.config["SESSION_TYPE"] = "filesystem"
app.config["UPLOAD_FOLDER"] = os.path.join(web_app, "build/tables/media/")
app.config["ALLOWED_EXTENSIOSN"] = {"png", "jpg", "jpeg", "gif"}
app.config["ENV"] = "production" if os.environ.get("HTTP_HOST", None) == "loadbusinessclub.com" else "dev"
app.config.from_object("loadbusinessclub.config.Config")
app.secret_key = app.config["SECRET_KEY"]

db = SQLAlchemy(app)
migrate = Migrate(app, db)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "auth.login"

from loadbusinessclub.views import main
from loadbusinessclub.auth.views import auth, authenticate, identity
from loadbusinessclub.admin.views import admin
from loadbusinessclub.history.views import history
from loadbusinessclub.shop.views import shop
from loadbusinessclub.transaction.views import tx
from loadbusinessclub.wallet.views import wallet

app.register_blueprint(auth)
app.register_blueprint(admin)
app.register_blueprint(main)
app.register_blueprint(history)
app.register_blueprint(shop)
app.register_blueprint(tx)
app.register_blueprint(wallet)

jwt = JWTManager(app)

from loadbusinessclub import models
setup_logging()


@login_manager.user_loader
def load_user(id):
    return models.User.query.get(int(id))


@basicauth.verify_password
def verify_password(token, password):
    user = models.User.verify_auth_token(token)
    g.user = user
    return True


@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    return response


@app.before_request
def before_request():
    if request.url.startswith("http://") and app.config["ENV"] == "production":
        url = request.url.replace("http://", "https://", 1)
        code = 301
        return redirect(url, code=code)

