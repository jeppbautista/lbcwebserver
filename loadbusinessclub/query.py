def genealogy():
    return """ 
        SELECT 
            '{username}' AS main,
            t1.username AS lvl1,
            t1.sponsorid AS lvl1_sponsor,
            t1.created AS lvl1_created,
            t2.username AS lvl2, 
            t2.sponsorid AS lvl2_sponsor,
            t2.created AS lvl2_created,
            t3.username AS lvl3,
            t3.sponsorid AS lvl3_sponsor,
            t3.created AS lvl3_created,
            t4.username AS lvl4,
            t4.sponsorid AS lvl4_sponsor, 
            t4.created AS lvl4_created,
            t5.username AS lvl5,
            t5.sponsorid AS lvl5_sponsor,
            t5.created AS lvl5_created,
            t6.username AS lvl6,
            t6.sponsorid AS lvl6_sponsor,
            t6.created AS lvl6_created,
            t7.username AS lvl7,
            t7.sponsorid AS lvl7_sponsor,
            t7.created AS lvl7_created
        FROM 
        (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            AND 	sponsorid={id}
            ORDER BY sponsorid
        )
        AS t1
        LEFT JOIN (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t2 
        ON t1.id = t2.sponsorid
        LEFT JOIN (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t3 
        ON t2.id = t3.sponsorid
        LEFT JOIN (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t4 
        ON t3.id = t4.sponsorid
        LEFT JOIN (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t5 
        ON t4.id = t5.sponsorid
        LEFT JOIN (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1 
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t6 
        ON t5.id = t6.sponsorid
        LEFT JOIN (
            SELECT  *
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t7 
        ON t6.id = t7.sponsorid  
        """


def singleline():
    return """ 
        SELECT *,
          CASE
            WHEN a.level = 1 THEN 15
            WHEN a.level = 2 THEN 17.50
            WHEN a.level = 3 THEN 20
            WHEN a.level = 4 THEN 30
            WHEN a.level = 5 THEN 45
            WHEN a.level = 6 THEN 62.50
            WHEN a.level = 7 THEN 107.50
            WHEN a.level = 8 THEN 157.50
            WHEN a.level = 9 THEN 300
            WHEN a.level = 10 THEN 450
            WHEN a.level = 11 THEN 625
            WHEN a.level = 12 THEN 825
            WHEN a.level = 13 THEN 1050
            WHEN a.level = 14 THEN 1300
            WHEN a.level = 15 THEN 1575
            WHEN a.level = 16 THEN 1875
            WHEN a.level = 17 THEN 2000
            WHEN a.level = 18 THEN 2250
            WHEN a.level = 19 THEN 2500
            WHEN a.level = 20 THEN 3000
            ELSE 0
          END AS dailyreward,
          CASE
            WHEN a.level = 1 THEN 150
            WHEN a.level = 2 THEN 350
            WHEN a.level = 3 THEN 600
            WHEN a.level = 4 THEN 1200
            WHEN a.level = 5 THEN 2250
            WHEN a.level = 6 THEN 3750
            WHEN a.level = 7 THEN 7500
            WHEN a.level = 8 THEN 15000
            WHEN a.level = 9 THEN 30000
            WHEN a.level = 10 THEN 54000
            WHEN a.level = 11 THEN 93750
            WHEN a.level = 12 THEN 148500
            WHEN a.level = 13 THEN 220000
            WHEN a.level = 14 THEN 325000
            WHEN a.level = 15 THEN 465750
            WHEN a.level = 16 THEN 637000
            WHEN a.level = 17 THEN 850000
            WHEN a.level = 18 THEN 1100000
            WHEN a.level = 19 THEN 1400000
            WHEN a.level = 20 THEN 1625000
            ELSE 0
          END AS maxreward
        FROM (
          SELECT  *, 
            CASE
              WHEN main.directactive >= 210 AND main.teamactive >= 1000000 THEN 20
              WHEN main.directactive >= 190 AND main.teamactive >= 715000 THEN 19
              WHEN main.directactive >= 171 AND main.teamactive >= 515000 THEN 18
              WHEN main.directactive >= 153 AND main.teamactive >= 365000 THEN 17
              WHEN main.directactive >= 136 AND main.teamactive >= 255000 THEN 16
              WHEN main.directactive >= 120 AND main.teamactive >= 175000 THEN 15
              WHEN main.directactive >= 105 AND main.teamactive >= 115000 THEN 14
              WHEN main.directactive >= 91 AND main.teamactive >= 70000 THEN 13
              WHEN main.directactive >= 78 AND main.teamactive >= 40000 THEN 12
              WHEN main.directactive >= 66 AND main.teamactive >= 20000 THEN 11
              WHEN main.directactive >= 55 AND main.teamactive >= 10000 THEN 10
              WHEN main.directactive >= 45 AND main.teamactive >= 5000 THEN 9
              WHEN main.directactive >= 36 AND main.teamactive >= 2500 THEN 8
              WHEN main.directactive >= 28 AND main.teamactive >= 1000 THEN 7
              WHEN main.directactive >= 21 AND main.teamactive >= 500 THEN 6
              WHEN main.directactive >= 15 AND main.teamactive >= 200 THEN 5
              WHEN main.directactive >= 10 AND main.teamactive >= 100 THEN 4
              WHEN main.directactive >= 6 AND main.teamactive >= 50 THEN 3
              WHEN main.directactive >= 3 AND main.teamactive >= 25 THEN 2
              WHEN main.directactive >= 1 AND main.teamactive >= 10 THEN 1
              ELSE 0
            END AS level
          FROM (
            SELECT id, username, created,
              (SELECT COUNT(1) FROM user WHERE x.id=sponsorid) as direct, 
              (SELECT COUNT(1) FROM user WHERE x.id=sponsorid AND is_activated=1) as directactive,
              (SELECT COUNT(1) FROM user WHERE x.id=sponsorid AND is_activated=0) as directinactive,
              (SELECT COUNT(1) FROM user WHERE x.created < created) as team,
              (SELECT COUNT(1) FROM user b WHERE x.created < b.created AND b.is_activated=1 ) as teamactive,
              (SELECT COUNT(1) FROM user b WHERE x.created < b.created AND (SELECT COUNT(1) FROM user WHERE b.id=sponsorid) < 1 ) as teaminactive
            FROM user x
            WHERE id={}
          ) as main
        ) as a
        """


def unilevel():
    return """ 
        SELECT 
            '{username}' as main,
            t1.username AS lvl1
        FROM 
        (
            SELECT  id,
                    username,
                    sponsorid 
            FROM    (SELECT * FROM user
                     WHERE is_activated = 1
                     ORDER BY created
                     ) products_sorted,
                    (SELECT @pv := '{id}') initialisation
            WHERE   find_in_set(sponsorid, @pv)
            AND     length(@pv := concat(@pv, ',', id))
            ORDER BY sponsorid
        )
        AS t1
        """


def unilevel_r():
    return """
        SELECT *
        FROM (
        select  id,
                username,
                sponsorid
        from    (select * from user
                 order by created DESC) products_sorted,
                (select @pv := '{id}') initialisation
        where   find_in_set(id, @pv)
        and     length(@pv := concat(@pv, ',', sponsorid))
        ) x
        """

