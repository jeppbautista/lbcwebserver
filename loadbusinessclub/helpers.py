import base64
import datetime


def generate_activation(id):
    timestamp = datetime.datetime.now().strftime("H%M%S%f")
    timestamp = timestamp+str(id)
    activation_bytes = timestamp.encode("ascii")

    base64_bytes = base64.b64encode(activation_bytes)
    base64_string = base64_bytes.decode("ascii").replace("=","5")
    return base64_string


def encode_sponsor(userid, salt="lbc"):
    newuid = salt+str(userid).zfill(3)
    newuid_byte = newuid.encode("utf-8")
    b64_uid = base64.b64encode(newuid_byte)
    return b64_uid.decode("utf-8")


def decode_sponsor(b64_uid, salt="lbc"):
    b64uid_bytes = b64_uid.encode("utf-8")
    olduid_bytes = base64.b64decode(b64uid_bytes)
    olduid = olduid_bytes.decode("utf-8")
    return int(olduid.replace(salt, ""))

