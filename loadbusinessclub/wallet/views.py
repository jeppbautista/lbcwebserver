from flask import Blueprint, jsonify, redirect, render_template, request, url_for, g, session
from flask_httpauth import HTTPBasicAuth

import decimal
import traceback

from loadbusinessclub import app, db
from loadbusinessclub.auth.views import token_required
from loadbusinessclub.models import *

wallet = Blueprint("wallet", __name__)
auth = HTTPBasicAuth()


@wallet.route("/api/balance", methods=["GET", "POST"])
@token_required
def balance():
    try:
        userid = g.user.id
        walletbalance = Wallet.query.filter_by(user=userid).first()
        walletbalance.balance = decimal.Decimal(walletbalance.singleline_earning) \
            + decimal.Decimal(walletbalance.unilevel_earning) \
            + decimal.Decimal(walletbalance.ecomm_earning)

        txs = TransactionWithdraw.query.filter(TransactionWithdraw.requestedby == userid).all()
        withdrawn = sum([decimal.Decimal(tx.amount) for tx in txs])

        walletbalance.withdrawn = withdrawn

        return jsonify(walletbalance.tojson()), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@wallet.route("/api/transfer", methods=["GET", "POST"])
@token_required
def transfer():
    try:
        data = request.get_json()
        subwallet = data["wallet_type"]
        amount = data["amount"]
        usernameto = data["usernameto"]
        userid = g.user.id

        vw = ViewSingleLine.query.filter(ViewSingleLine.id == userid).first()
        level = vw.level

        if level < 5:
            return jsonify(status="Level 5 is required"), 500

        userto = User.query.filter(User.username == usernameto).first()
        userfrom = User.query.filter(User.id == userid).first()
        walletto = Wallet.query.filter(Wallet.user == userto.id).first()
        walletfrom = Wallet.query.filter(Wallet.user == userfrom.id).first()

        amount = decimal.Decimal(amount)

        if subwallet == "SINGLELINE":
            if amount <= walletfrom.singleline_earning:
                walletfrom.singleline_earning = decimal.Decimal(walletfrom.singleline_earning) - decimal.Decimal(amount)
                walletto.singleline_earning = decimal.Decimal(walletto.singleline_earning) + \
                                                (decimal.Decimal(amount) - decimal.Decimal(app.config["TRANSFER_FEE"]))

                db.session.add(
                    TransactionTransfer(
                        amount=amount,
                        walletfrom=walletfrom.id,
                        walletto=walletto.id,
                        subwallet=subwallet
                    )
                )

                db.session.commit()
            else:
                return jsonify(status="Invalid amount")
        elif subwallet == "UNILEVEL":
            if amount <= walletfrom.unilevel_earning:
                walletfrom.unilevel_earning = decimal.Decimal(walletfrom.unilevel_earning) - decimal.Decimal(amount)
                walletto.unilevel_earning = decimal.Decimal(walletto.unilevel_earning) + \
                                            (decimal.Decimal(amount) - decimal.Decimal(app.config["TRANSFER_FEE"]))

                db.session.add(
                    TransactionTransfer(
                        amount=amount,
                        walletfrom=walletfrom.id,
                        walletto=walletto.id,
                        subwallet=subwallet
                    )
                )

                db.session.commit()
            else:
                return jsonify(status="Invalid amount")

        elif subwallet == "ECOMMERCE":
            if amount <= walletfrom.ecomm_earning:
                walletfrom.ecomm_earning = decimal.Decimal(walletfrom.ecomm_earning) - decimal.Decimal(amount)
                walletto.ecomm_earning = decimal.Decimal(walletto.ecomm_earning) + decimal.Decimal(amount)

                db.session.add(
                    TransactionTransfer(
                        amount=amount,
                        walletfrom=walletfrom.id,
                        walletto=walletto.id,
                        subwallet=subwallet
                    )
                )

                db.session.commit()
        else:
            return jsonify(status="Something went wrong", message="invalid subwallet")

        return jsonify(status="Successful")
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@wallet.route("/api/withdraw", methods=["GET", "POST"])
@token_required
def withdraw():
    try:
        userid = g.user.id
        data = request.get_json()
        wallet_type = data["wallet_type"]
        pay_type = data["pay_type"]
        account_name = data["account_name"]
        account_number = data["account_number"]
        amount = decimal.Decimal(data["amount"])
        balance = decimal.Decimal(data["balance"])

        user = User.query.filter(User.id == userid).first()
        vw = ViewSingleLine.query.filter(ViewSingleLine.id == userid).first()
        wallet = Wallet.query.filter(Wallet.user == user.id).first()
        # level = vw.level
        #
        # if level < 5:
        #     return jsonify(status="Level 5 is required"), 500

        if pay_type == "SINGLELINE" and amount > vw.maxreward:
            return jsonify(status="Amount is exceeding your maximum reward"), 500

        if amount < 500:
            return jsonify(status="Minimum withdrawal is 500PHP"), 500

        if amount > balance:
            return jsonify(status="Insufficient balance"), 500

        db.session.add(
            TransactionWithdraw(
                username=user.username,
                amount=amount,
                balance=balance,
                requestedby=user.id,
                paytype=pay_type,
                accountnumber=account_number,
                accountname=account_name,
                wallet=wallet.id,
                wallettype=wallet_type,
                status="PENDING"
            )
        )
        db.session.commit()

        return jsonify(status="Successful"), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500