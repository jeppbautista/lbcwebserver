from loadbusinessclub.models import User

def get_user(**kv):
    user = User.query.filter_by(**kv).first()
    return user
