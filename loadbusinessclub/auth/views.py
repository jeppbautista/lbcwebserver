from functools import wraps

from flask import Blueprint, jsonify, render_template, request, g
from flask_jwt_extended import create_access_token, decode_token, jwt_required
from flask_login import logout_user
from sqlalchemy.exc import IntegrityError
import traceback

from loadbusinessclub import app, db, basicauth, g
from loadbusinessclub.models import User, UserDetails, Wallet
from loadbusinessclub.auth.query import get_user
from loadbusinessclub.auth.forms import Login, Signup
from loadbusinessclub.helpers import decode_sponsor, encode_sponsor

from werkzeug.datastructures import Authorization

import os
auth = Blueprint("auth", __name__)


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None

        if "access_token" in request.get_json().keys():
            token = request.get_json()["access_token"]

        if not token:
            return jsonify(status="Invalid token")

        a = Authorization(True, {"username": token, "password": "unused"})
        basicauth.authenticate(a, None)

        return f(*args, **kwargs)
    return decorator


@auth.route("/api/auth/change-password", methods=["GET", "POST"])
@token_required
def change_password():
    try:
        data = request.get_json()
        old_password = data["oldPassword"]
        new_password = data["newPassword"]
        userid = g.user.id

        user = User.query.filter(User.id == userid).first()
        if user.validate_password(old_password):
            formdata = {
                "username": user.username,
                "password": new_password
            }

            loginform = Login.from_json(formdata)

            if loginform.validate():
                user.password = new_password
                db.session.commit()
                return jsonify(status="Done"), 200
            else:
                app.logger.error("Invalid form")
                return jsonify(status="Invalid form", errors=loginform.errors), 400

        else:
            app.logger.error("Password is incorrect")
            return jsonify(status="Password is incorrect"), 500

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@auth.route("/auth/login")
def login():
    return render_template("index.html")


@auth.route("/api/auth/submit-login", methods=["GET", "POST"])
def submit_login():
    data = request.get_json()
    if not data:
        return jsonify(status="Invalid form"), 400

    form = Login.from_json(data)
    if form.validate():
        user = get_user(username=form.username.data)
        if user is not None and user.validate_password(form.password.data):
            # access_token = create_access_token(identity=user.username)
            access_token = user.generate_auth_token()
            g.user = user
            return jsonify(status="Logged in", access_token=access_token.decode("ascii")), 200
        else:
            return jsonify(status="Invalid credentials"), 401
    else:
        return jsonify(status="Invalid form", errors=form.errors), 400


@auth.route("/api/auth/submit-signup", methods=["GET", "POST"])
def submit_signup():
    data = request.get_json()

    data["sponsor"] = data.get("sponsor", "admin")
    if not data["sponsor"]:
        data["sponsor"] = "admin"

    user = User.query.filter(User.username == data["sponsor"]).first()
    data["sponsor"] = user.id

    if not data.get("country", None):
        data["country"] = "NA"

    #TODO bug on country

    if data.get("country", None) != "PHL":
        data["city"] = data.get("country", None)
        data["province"] = data.get("country", None)

    form = Signup.from_json(data)
    if form.validate():
        try:
            user = User(
                username=form.username.data,
                usertypeid=1,
                password=form.password.data,
                sponsorid=form.sponsor.data
            )
            db.session.add(user)
            db.session.commit()

            wallet = Wallet(user=user.id)
            db.session.add(wallet)
            db.session.commit()

            userdetails = UserDetails(
                user=user.id,
                name=form.name.data,
                email=form.email.data,
                country=form.country.data,
                province=form.province.data,
                city=form.city.data
            )
            db.session.add(userdetails)
            db.session.commit()
        except IntegrityError as e:
            app.logger.error(traceback.format_exc())
            return jsonify(status="Database error"), 500
        except Exception as e:
            app.logger.error(traceback.format_exc())
            return jsonify(status="Something went wrong"), 500

        return jsonify(status="Signup successful"), 200
    else:
        app.logger.error(form.errors)
        return jsonify(status="Invalid form", errors=form.errors), 400


@auth.route("/api/auth/submit-signup-admin", methods=["GET", "POST"])
def submit_signup_admin():
    app.logger.error("")
    data = request.get_json()

    if data.get("country", None) != "PHILIPPINES":
        data["city"] = data.get("country", None)
        data["province"] = data.get("country", None)

    form = Signup.from_json(data)
    if form.validate():
        try:
            u = User.query.filter(User.username == data["sponsorusername"]).first()
            user = User(
                username=form.username.data,
                usertypeid=1,
                password=form.password.data,
                sponsorid=u.id
            )
            db.session.add(user)
            db.session.commit()

            wallet = Wallet(user=user.id)
            db.session.add(wallet)
            db.session.commit()

            userdetails = UserDetails(
                user=user.id,
                name=form.name.data,
                email=form.email.data,
                country=form.country.data,
                province=form.province.data,
                city=form.city.data
            )
            db.session.add(userdetails)
            db.session.commit()
        except IntegrityError as e:
            return jsonify(status="Database error"), 500
        except Exception as e:
            return jsonify(status="Something went wrong"), 500

        return jsonify(status="Signup successful"), 200
    else:
        app.logger.error(form.errors)
        return jsonify(status="Invalid form", errors=form.errors), 400


@auth.route("/api/auth/logout")
def logout():
    logout_user()
    return jsonify(status="Logged out"), 200


def authenticate(username, password):
    try:
        user = User.query.filter_by(username=username).first()
        if user.validate_password(password):
            return user
    except Exception as e:
        app.logger.error(traceback.format_exc())
    return False


def identity(payload):
    user_id = payload["identity"]
    return User.query.get(user_id)



@auth.route("/foobar", methods=["POST", "GET"])
@token_required
def foobar():
    return str(g.user)



