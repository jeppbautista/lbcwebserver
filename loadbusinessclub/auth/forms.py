from flask_wtf import FlaskForm
import wtforms as wtf
from wtforms import validators


class Login(FlaskForm):
    class Meta:
        csrf = False

    username = wtf.StringField(
        "Username",
        validators=[
            validators.InputRequired(),
            validators.Length(min=4, max=30),
        ]
    )

    password = wtf.PasswordField(
        "Password",
        validators=[
            validators.InputRequired(),
            validators.Length(min=6, max=30)
        ]
    )


class Signup(FlaskForm):
    class Meta:
        csrf = False

    username = wtf.StringField(
        "Username",
        validators=[
            validators.InputRequired(),
            validators.Length(min=4, max=30),
        ]
    )

    password = wtf.PasswordField(
        "Password",
        validators=[
            validators.InputRequired(),
            validators.Length(min=4, max=30),
            validators.EqualTo("confirm", message="Password must match")
        ]
    )

    confirm = wtf.PasswordField("Confirm password")

    name = wtf.StringField(
        "Name",
        validators=[
            validators.InputRequired(),
            validators.Length(min=3, max=50),
        ]
    )

    phone = wtf.StringField(
        "Phone"
    )

    email = wtf.StringField(
        "Email",
        validators=[
            validators.Email(),
            validators.InputRequired()
        ]
    )

    country = wtf.StringField(
        "Country",
        validators=[
            validators.InputRequired()
        ]
    )

    province = wtf.StringField(
        "Province",
        validators=[
            validators.InputRequired()
        ])

    city = wtf.StringField(
        "Municipality/City",
        validators=[
            validators.InputRequired()
        ]
    )

    sponsor = wtf.StringField("Sponsor", default=1)



