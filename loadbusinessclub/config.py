from loadbusinessclub import app


class ProdConfig:
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://loadbusi_admin:{LBCPASS}@64.20.48.184:3306/loadbusi_prod".format(
        LBCPASS="r76LqvLEbn4MQJc"
    )


class DevConfig:
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:osboxes.org@localhost:3306/loadbusi_test".format(
        LBCPASS="r76LqvLEbn4MQJc"
    )


class Config(ProdConfig if app.config["ENV"] == "production" else DevConfig):
    TEST = ""
    SECRET_KEY = "3ec3322b15aabb4031d6742664eb267dbe3185d3"
    JWT_SECRET_KEY = "3ec3322b15aabb4031d6742664eb267dbe3185d3"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SESSION_COOKIE_SECURE = True
    REMEMBER_COOKIE_SECURE = True
    SESSION_COOKIE_HTTPONLY = True
    REMEMBER_COOKIE_HTTPONLY = True
    UNILEVEL_EARNING = {
        1: 10.0,
        2: 5.0,
        3: 2.0,
        4: 2.0,
        5: 2.0,
        6: 2.0,
        7: 2.0
    }
    TRANSFER_FEE = 10
    COMMUNITY_FUND = 0.1
    STOCKIST_DEALER = 0
    STOCKIST_MOBILE = 0.15
    STOCKIST_CITY = 0.35
    STOCKIST_PROVINCIAL = 0.5

    EMAIL_ADMIN = "slapadasbas@gmail.com"
    EMAIL_AUTO = "do-not-reply@loadbusinessclub.com"
    EMAIL_PASS = "r76LqvLEbn4MQJc"
