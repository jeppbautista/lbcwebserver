from flask_wtf import FlaskForm
import wtforms as wtf
from wtforms import validators


class Purchase(FlaskForm):
    class Meta:
        csrf = False

    customer = wtf.StringField("CustomerID")
    shipdate = wtf.DateTimeField("Ship Date")
    requireddate = wtf.DateTimeField("Required Date")
    status = wtf.StringField("Status", default="ACCEPTED")
    err = wtf.StringField("Error")
    paymentdate = wtf.DateTimeField("Payment Date")
    receipt = wtf.TextAreaField("Receipt")

    is_deleted = wtf.BooleanField(default=False)
    is_paid = wtf.BooleanField(default=False)

    ordernumber = wtf.StringField("Order Number")

    payment = wtf.IntegerField(
        "PaymentID",
        validators=[
            validators.InputRequired()
        ],
        default=1
    )

    fullname = wtf.StringField(
        "Full name",
        validators=[
            validators.InputRequired()
        ]
    )

    address = wtf.StringField(
        "Address",
         validators=[
             validators.InputRequired()
         ]
    )

    city = wtf.StringField(
        "City",
        validators=[
             validators.InputRequired()
         ]
    )

    province = wtf.StringField(
        "Province",
        validators=[
            validators.InputRequired()
        ]
    )

    phone = wtf.StringField(
        "Phone",
        validators=[
            validators.InputRequired()
        ]
    )
    email = wtf.StringField(
        "Email",
        validators=[
            validators.InputRequired()
        ]
    )

