from flask import Blueprint, jsonify, render_template, request, g
from sqlalchemy.exc import IntegrityError

import datetime
import decimal
import os
import re
import traceback

from loadbusinessclub import app, db, query
from loadbusinessclub.auth.views import token_required
from loadbusinessclub.helpers import generate_activation
from loadbusinessclub.models import *
from loadbusinessclub.shop.forms import Purchase

shop = Blueprint("shop", __name__)


@shop.route("/api/shop/buy-activation", methods=["POST", "GET"])
@token_required
def buy_activation():
    try:
        userid = g.user.id
        data = request.get_json()
        data["city"] = "NA"
        data["province"] = "NA"
        order_details = data["order_details"]
        del data["order_details"]
        form = Purchase.from_json(data)

        if form.validate():
            try:
                grandtotal = 250 * order_details["products"][0]["qty"]
                grandtotal = grandtotal + (grandtotal*0.1)

                if data["payment"] == 7:
                    vw = ViewSingleLine.query.filter(ViewSingleLine.id == userid).first()
                    level = vw.level

                    if level < 2:
                        return jsonify(status="Atleast level 2 is required"), 500

                    wallet = Wallet.query.filter(Wallet.user == userid).first()
                    earning = decimal.Decimal(wallet.singleline_earning) - (decimal.Decimal(grandtotal))
                    if earning < 0:
                        app.logger.error("Insufficient balance")
                        return jsonify(status="Insufficient balance"), 500

                    # wallet.singleline_earning = earning

                customer = Customer(
                    fullname=form.fullname.data,
                    address=form.address.data,
                    phone=form.phone.data,
                    email=form.email.data
                )
                db.session.add(customer)
                db.session.commit()

                app.logger.error("INFO: Customer created: {}".format(customer.id))

                user_customer = UserCustomer(
                    user=g.user.id,
                    customer=customer.id
                )
                db.session.add(user_customer)

                last_ordid = Orders.query.order_by(Orders.id.desc()).first()
                ordid = last_ordid.id + 1 if last_ordid else 1
                ordernumber = datetime.datetime.now().strftime("%m%d") + str(customer.id) + str(
                    form.payment.data) + str(
                    ordid)

                order = Orders(
                    customer=customer.id,
                    payment=form.payment.data,
                    ordernumber=ordernumber,
                    status="PENDING",
                    receipt=form.receipt.data,
                    grand_total=grandtotal
                )
                db.session.add(order)
                db.session.commit()

                app.logger.error("INFO: UserCustomer created: {}".format(user_customer.id))
                app.logger.error("INFO: Order created: {}".format(order.id))


                for product in order_details["products"]:
                    db.session.add(
                        OrderDetail(
                            order=order.id,
                            product=product["product"],
                            unitprice=product["unitprice"],
                            subtotal=order_details["subtotal"],
                            discount=order_details["discount"],
                            lbctax=order_details["lbctax"],
                            qty=product["qty"],
                            total=order_details["total"]
                        )
                    )


                db.session.commit()
                app.logger.error("INFO: Successful")
                return jsonify(status="Succesful"), 200

            except Exception as e:
                app.logger.error(traceback.format_exc())
                return jsonify(status="Something went wrong", message=str(e)), 500

        else:
            app.logger.error(form.errors)
            return jsonify(status="Invalid form", errors=form.errors), 400
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/orders", methods=["POST", "GET"])
@token_required
def orders():
    def _orders(order):
        return {
            "order_detail": order[0].tojson(),
            "product": order[1].tojson(),
            "order": order[2].tojson(),
            "customer": order[3].tojson()
        }

    try:
        userid = g.user.id
        ords = db.session.query(OrderDetail, Product, Orders, Customer) \
            .join(Product, OrderDetail.product == Product.id) \
            .join(Orders, OrderDetail.order == Orders.id) \
            .join(Customer, Orders.customer == Customer.id) \
            .filter(Product.merchant == userid) \
            .filter(Orders.status == "ACCEPTED") \
            .filter(OrderDetail.status == "ACCEPTED") \
            .all()
        return jsonify([_orders(ord) for ord in ords])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/ships", methods=["POST", "GET"])
@token_required
def ships():
    def _orders(order):
        return {
            "order_detail": order[0].tojson(),
            "product": order[1].tojson(),
            "order": order[2].tojson(),
            "customer": order[3].tojson()
        }

    try:
        userid = g.user.id
        ords = db.session.query(OrderDetail, Product, Orders, Customer) \
            .join(Product, OrderDetail.product == Product.id) \
            .join(Orders, OrderDetail.order == Orders.id) \
            .join(Customer, Orders.customer == Customer.id) \
            .filter(Product.merchant == userid) \
            .filter(Orders.status == "ACCEPTED") \
            .filter(OrderDetail.status == "SHIPPING") \
            .all()
        return jsonify([_orders(ord) for ord in ords])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/order/to-ship", methods=["POST", "GET"])
@token_required
def to_ship():
    try:
        data = request.get_json()
        order_detail_id = data["order_detail"]

        order_detail = OrderDetail.query.filter(OrderDetail.id == order_detail_id).first()

        order_detail.status = "SHIPPING"

        db.session.commit()

        return jsonify(status="Successful")
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/order/to-receive", methods=["POST", "GET"])
@token_required
def to_receive():
    try:
        data = request.get_json()
        userid = g.user.id
        order_detail_id = data["order_detail"]

        order_detail = OrderDetail.query.filter(OrderDetail.id == order_detail_id).first()
        order_detail.status = "RECEIVED"

        net_sales = decimal.Decimal(order_detail.subtotal)
        community_fund = decimal.Decimal(order_detail.subtotal) * decimal.Decimal(app.config["COMMUNITY_FUND"])
        gross_sales = net_sales - community_fund

        merchantwallet = Wallet.query.filter(Wallet.user == userid).first()
        merchantwallet.ecomm_earning = decimal.Decimal(merchantwallet.ecomm_earning) + decimal.Decimal(gross_sales)

        db.session.add(
            TransactionEcommerce(
                user=userid,
                amount=gross_sales,
                earningtype="SALE",
                earningfrom=userid,
                order_detail=order_detail_id
            )
        )

        order = Orders.query.filter(Orders.id == order_detail.order).first()
        customer = UserCustomer.query.filter(UserCustomer.customer == order.customer).first()

        if customer:

            sql = query.unilevel_r().format(id=customer.user)

            colnames = ["id", "username", "sponsorid"]

            results = db.session.execute(sql)
            result = [row for row in results]
            unilevel = [{colnames[col]: x for col, x in enumerate(r)} for r in result]

            for i in range(len(unilevel)):
                level = i
                unilevel[i]["level"] = level

            final = [uni for uni in unilevel[:8]]

            community_fund = float(community_fund)
            unilevel_earning = community_fund * .35
            stockists = community_fund * .3
            db.session.commit()

            try:

                level_earning = unilevel_earning / 7

                for fin in final[1:]:
                    wallet = db.session.query(Wallet, User, UserType) \
                        .join(User, Wallet.user == User.id) \
                        .join(UserType, User.usertypeid == UserType.id) \
                        .filter(Wallet.user == fin["id"]).first()

                    stockist_income = stockists * app.config["STOCKIST_DEALER"] if wallet.UserType.usertype == "DEALER"\
                        else stockists * app.config["STOCKIST_MOBILE"] if wallet.UserType.usertype == "MOBILE"\
                        else stockists * app.config["STOCKIST_CITY"] if wallet.UserType.usertype == "CITY"\
                        else stockists * app.config["STOCKIST_PROVINCIAL"] if wallet.UserType.usertype == "PROVINCIAL"\
                        else 0

                    wallet.Wallet.ecomm_earning = decimal.Decimal(wallet.Wallet.ecomm_earning) + \
                        decimal.Decimal(level_earning) + \
                        decimal.Decimal(stockist_income)

                    db.session.add(
                        TransactionEcommerce(
                            user=fin["id"],
                            amount=decimal.Decimal(level_earning) + decimal.Decimal(stockist_income),
                            earningtype="UNILEVEL",
                            earningfrom=userid,
                            order_detail=order_detail_id
                        )
                    )
                    db.session.commit()
            except ZeroDivisionError as e:
                app.logger.error("Zero division")
                pass
            except Exception as e:
                app.logger.error(traceback.format_exc())
                return jsonify(status="Something went wrong", message=str(e)), 500

        return jsonify(status="Successful")
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/buy", methods=["POST", "GET"])
def buy():
    try:
        data = request.get_json()
        order_details = data["order_details"]
        del data["order_details"]
        access_token = data.get("access_token", None)

        form = Purchase.from_json(data)
        if form.validate():
            try:
                customer = Customer(
                    fullname=form.fullname.data,
                    address=form.address.data,
                    phone=form.phone.data,
                    email=form.email.data,
                    city=form.city.data,
                    province=form.province.data
                )
                db.session.add(customer)
                db.session.commit()

                app.logger.error("INFO: Customer created: {}".format(customer.id))

                if access_token:
                    user = User.verify_auth_token(access_token)
                    if user:
                        user_customer = UserCustomer(
                            user=user.id,
                            customer=customer.id
                        )
                        db.session.add(user_customer)
                        db.session.commit()
                        app.logger.error("INFO: UserCustomer created: {}".format(user_customer.id))

                last_ordid = Orders.query.order_by(Orders.id.desc()).first()
                ordid = last_ordid.id + 1 if last_ordid else 1
                ordernumber = datetime.datetime.now().strftime("%m%d") + str(customer.id) + str(
                    form.payment.data) + str(
                    ordid)

                order = Orders(
                    customer=customer.id,
                    payment=form.payment.data,
                    ordernumber=ordernumber,
                    status="PENDING",
                    receipt=form.receipt.data,
                    paymentdate=datetime.datetime.now() if form.receipt.data else None,
                    is_paid=1 if form.receipt.data else 0
                )
                db.session.add(order)
                db.session.commit()

                app.logger.error("INFO: Order created: {}".format(order.id))

                grandtotal = 0
                for product in order_details["products"]:
                    grandtotal += product["subtotal"] + (
                        product["shippingmm"] if customer.province.upper() == "METRO MANILA" else
                        product["shippingnonmm"]
                    )
                    db.session.add(
                        OrderDetail(
                            order=order.id,
                            product=product["id"],
                            unitprice=product["discountprice"] if access_token else product["unitprice"],
                            subtotal=product["subtotal"],
                            discount=(product["unitprice"] - product["discountprice"]) if access_token else 0,
                            lbctax=0,
                            qty=product["qty"],
                            total=order_details["total"],
                            shippingfee=product["shippingmm"] if customer.province.upper() == "METRO MANILA" else product["shippingnonmm"]
                        )
                    )

                order.grand_total = grandtotal

                db.session.commit()
                app.logger.error("INFO: Successful")
                return jsonify(status="Succesful"), 200
            except Exception as e:
                app.logger.error(traceback.format_exc())
                return jsonify(status="Something went wrong", message=str(e)), 500
        else:
            app.logger.error(form.errors)
            return jsonify(status="Invalid form", errors=form.errors), 400
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/update-shipping", methods=["POST", "GET"])
@token_required
def update_shipping():
    try:
        data = request.get_json()
        userid = g.user.id

        user = User.query.filter(User.id == userid).first()
        user.shippingmm = data["shippingfeemm"]
        user.shippingnonmm = data["shippingfeenonmm"]

        products = Product.query.filter(Product.merchant == userid).all()

        for product in products:
            product.shippingmm = data["shippingfeemm"]
            product.shippingnonmm = data["shippingfeenonmm"]

        db.session.commit()

        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/products/<category>/<limit>/<offset>")
def products(category, limit=0, offset=0):
    try:
        if category == "ALL":
            items = Product.query \
                .filter(Product.is_deleted == False) \
                .filter(Product.is_available == True)\
                .offset(offset)\
                .limit(limit)\
                .all()
        else:
            category = re.sub(r"(?<=\w)([A-Z])", r" \1", category)
            category = category.replace("And", "&")
            items = Product.query \
                .filter(Product.is_deleted == False) \
                .filter(Product.is_available == True) \
                .filter(Product.category == category) \
                .offset(offset) \
                .limit(limit) \
                .all()

        return jsonify([item.tojson() for item in items]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/products/test")
def test():
    try:
        items = Product.query.all()
        return jsonify([item.tojson() for item in items]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/my-products", methods=["GET", "POST"])
@token_required
def my_products():
    try:
        userid = g.user.id
        items = Product.query.filter(Product.merchant == userid)\
            .filter(Product.is_deleted == False)\
            .filter(Product.is_available == True)\
            .all()
        return jsonify([item.tojson() for item in items]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/my-products-request", methods=["GET", "POST"])
@token_required
def my_product_request():
    def _product(item):
        return {
            "product": item.Product.tojson(),
            "user": item.User.tojson()
        }

    try:
        userid = g.user.id
        items = db.session.query(Product, User)\
            .join(User, Product.merchant == User.id)\
            .filter(Product.is_deleted == False)\
            .filter(Product.is_available == False)\
            .all()

        return jsonify([_product(item) for item in items]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/quota-request", methods=["GET", "POST"])
@token_required
def quota_request():
    try:
        userid = g.user.id
        data = request.get_json()

        count = data["count"]
        amount = data["amount"]
        image = data["image"]

        db.session.add(
            TransactionQuota(
                user=userid,
                count=count,
                amount=amount,
                status="PENDING",
                receipt=image
            )
        )

        db.session.commit()

        return jsonify(status="Successful"), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/add", methods=["GET", "POST"])
@token_required
def product_add():
    try:
        userid = g.user.id

        data = request.get_json()
        productid = data["productid"]
        category = data["category"]
        productname = data["productname"]
        productdescription = data.get("productdescription", "")
        unitprice = data["unitprice"]
        discountprice = unitprice \
            if not data.get("discountprice", None) else float(data.get("discountprice", 0)) \
            if float(data.get("discountprice", 0)) > 0 else unitprice

        user = User.query.filter(User.id == userid).first()

        shippingmm = user.shippingmm
        shippingnonmm = user.shippingnonmm
        image = data.get("image", ["default.png"])
        if type(image) == list:
            image = (image + [None]*5)[:5]
        else:
            image = ["default.png", None, None, None, None]
        ranking = data["ranking"]

        merchant = User.query.filter(User.id == userid).first()

        if productid == 0:
            my_products = Product.query.filter(Product.merchant == userid).all()
            if len(my_products) < merchant.product_quota or (len(my_products) >= merchant.product_quota and userid == 1):

                db.session.add(
                    Product(
                        merchant=userid,
                        category=category,
                        productname=productname,
                        productdescription=productdescription,
                        unitprice=unitprice,
                        discountprice=discountprice,
                        shippingmm=shippingmm,
                        shippingnonmm=shippingnonmm,
                        image=image[0],
                        image2=image[1],
                        image3=image[2],
                        image4=image[3],
                        image5=image[4],
                        ranking=ranking
                    )
                )
            else:
                app.logger.error("Already at maximum products")
                return jsonify(status="Already at maximum products upload"), 500

        else:
            product = Product.query.filter(Product.id == productid).first()
            if product:
                product.category = category
                product.productname = productname
                product.productdescription = productdescription
                product.unitprice = unitprice
                product.discountprice = discountprice
                product.shippingmm = shippingmm
                product.shippingnonmm = shippingnonmm
                product.image = image[0],
                product.image2 = image[1],
                product.image3 = image[2],
                product.image4 = image[3],
                product.image5 = image[4],

        db.session.commit()

        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/delete", methods=["GET", "POST"])
@token_required
def product_delete():
    try:
        data = request.get_json()

        productid = data["product"]
        product = Product.query.filter(Product.id == productid).first()
        product.is_deleted = True

        db.session.commit()

        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/<id>", methods=["GET", "POST"])
def product(id):
    def _product(prod):
        return {
            "product": prod[0].tojson(),
            "user": prod[1].tojson()
        }

    try:
        prod = db.session.query(Product, UserDetails) \
            .join(UserDetails, Product.merchant == UserDetails.user) \
            .filter(Product.id == id).first()
        print(prod)
        return jsonify(_product(prod)), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/available", methods=["GET", "POST"])
@token_required
def product_avail():
    try:
        data = request.get_json()
        productid = data["product"]
        is_available = data["is_available"]

        product = Product.query.filter(Product.id == productid).first()
        product.is_available = is_available

        db.session.commit()

        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/product/upload-picture", methods=["GET", "POST"])
def product_upload():
    try:

        if "file0" not in request.files:
            app.logger.error("No file part")
            return jsonify(status="No file part"), 500

        files = request.files
        filenames = []

        for k,f in request.files.items():
            productid = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")

            if f.filename == "":
                app.logger.error("No file selected")
                return jsonify(status="No file selected"), 500

            if f:
                extension = f.filename.split(".")[-1]
                filename = "product-{productid}.{extension}" \
                    .format(productid=productid, extension=extension)

                f.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
                app.logger.error(os.path.join(app.config["UPLOAD_FOLDER"], filename))

                filenames.append(filename)
        return jsonify(status="Success",
                        filepath=[os.path.join(app.config["UPLOAD_FOLDER"], filename) for filename in filenames],
                        filename=filenames), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/categories")
def categories():
    return jsonify(categories=["Electronics",
                               "Health",
                               "Clothing",
                               "Gadgets", "Beauty",
                               "Home & Living",
                               "Sports & Lifestyle",
                               "Automotive", "Food", "Toys",
                               "Others"
                               ])


@shop.route("/api/shop/paymenttype")
def paymenttype():
    try:
        paytypes = Payment.query.all()
        return jsonify([paytype.tojson() for paytype in paytypes])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@shop.route("/api/shop/my-orders", methods=["GET", "POST"])
@token_required
def my_orders():
    def _order(order):
        return {
            "order": order[0].tojson(),
            "customer": order[1].tojson(),
            "payment": order[2].tojson()
        }

    try:
        data = request.get_json()
        orders = db.session.query(Orders, Customer, Payment, OrderDetail) \
            .join(Customer, Orders.customer == Customer.id) \
            .join(Payment, Orders.payment == Payment.id) \
            .join(UserCustomer, Orders.customer == UserCustomer.customer) \
            .join(OrderDetail, Orders.id == OrderDetail.order) \
            .order_by(Orders.created.desc()) \
            .filter(UserCustomer.user == g.user.id)\
            .all()

        print(orders)

        return jsonify([_order(order) for order in orders if order.OrderDetail.product != 1]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500
