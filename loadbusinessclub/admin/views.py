from flask import Blueprint, jsonify, request, g
from flask_login import current_user

import decimal
import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import traceback

from loadbusinessclub import app, db, query
from loadbusinessclub.helpers import generate_activation
from loadbusinessclub.auth.views import token_required
from loadbusinessclub.models import *
from loadbusinessclub.models import _format_date

admin = Blueprint("admin", __name__)


@admin.route("/api/admin/reset-password", methods=["GET", "POST"])
@token_required
def reset_password():
    try:
        data = request.get_json()
        user = User.query.filter(User.id == data["user"]).first()
        user.password = "12345678"
        db.session.commit()

        return jsonify(status="Successful"), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/activations", methods=["GET", "POST"])
@token_required
def admin_activations():
    try:
        data = request.get_json()
        orderid = data["order"]

        codes = Activation.query.filter(Activation.order == orderid).all()

        return jsonify([code.activationcode for code in codes])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/generate-code", methods=["GET", "POST"])
@token_required
def generate_code():
    try:
        data = request.get_json()
        num = data["num"]
        codes = []
        for n in range(num):
            codes.append(generate_activation(n))

        return jsonify(codes)
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/singleline", methods=["GET", "POST"])
@token_required
def admin_activation():
    try:
        singleline = ViewSingleLine.query.all()
        return jsonify([single.tojson() for single in singleline])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/quota-request", methods=["GET", "POST"])
@token_required
def admin_quota():
    def _quota(q):
        return {
            "transaction_quota": q.TransactionQuota.tojson(),
            "user": q.User.tojson()
        }
    try:
        results = db.session.query(TransactionQuota, User)\
            .join(User, TransactionQuota.user == User.id)\
            .all()

        return jsonify([_quota(result) for result in results])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/user", methods=["GET", "POST"])
@token_required
def admin_user():

    try:
        users = ViewUser.query.order_by(ViewUser.created.desc()).all()
        return jsonify([user.tojson() for user in users])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/order", methods=["GET", "POST"])
@token_required
def orders():
    def _order(order):
        return {
            "id": order.id,
            "created": _format_date(order.created),
            "ordernumber": order.ordernumber,
            "grand_total": float(order.grand_total),
            "status": order.status,
            "receipt": order.receipt,
            "fullname": order.fullname
        }
    try:
        orders = Orders.query\
            .join(OrderDetail, OrderDetail.order == Orders.id) \
            .join(Customer, Orders.customer == Customer.id)\
            .add_columns(
                Orders.id,
                Orders.created,
                Orders.ordernumber,
                Orders.grand_total,
                Orders.status,
                Orders.receipt,
                Customer.fullname
            )\
            .order_by(Orders.created.desc())\
            .filter((Orders.status == "PENDING") | (Orders.status == "ACCEPTED"))\
            .filter(OrderDetail.product != 1).all()

        return jsonify([_order(order) for order in orders])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/payment", methods=["GET", "POST"])
@token_required
def payments():
    def _payment(order):
        return {
            "order": order[0].tojson(),
            "customer": order[1].tojson(),
            "payment": order[2].tojson()
        }
    try:
        orders = db.session.query(Orders, Customer, Payment, OrderDetail)\
            .join(Customer, Orders.customer == Customer.id)\
            .join(Payment,  Orders.payment == Payment.id)\
            .join(OrderDetail, Orders.id == OrderDetail.order)\
            .order_by(Orders.created.desc())\
            .filter((Orders.status == "PENDING") | (Orders.status == "ACCEPTED"))\
            .filter(OrderDetail.product == 1).all()

        return jsonify([_payment(order) for order in orders])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/payment-detail", methods=["GET", "POST"])
@token_required
def payment_details():
    def _payment_detail(order):
        return {
            "id_orderdetail": order.orderdetail_id,
            "id_product": order.product_id,
            "qty": order.qty,
            "subtotal": float(order.subtotal),
            "discount": float(order.discount),
            "lbctax": float(order.lbctax),
            "total": float(order.total),
            "merchant": order.merchant,
            "productname": order.productname,
            "productdescription": order.productdescription,
            "unitprice": float(order.unitprice),
            "image": order.image,
            "ranking": order.ranking,
            "is_deleted": order.is_deleted
        }

    try:
        data = request.get_json()
        orderid = data.get("order", None)
        order_details = OrderDetail.query \
            .join(Product, OrderDetail.product == Product.id) \
            .add_columns(
                OrderDetail.id.label("orderdetail_id"),
                OrderDetail.qty,
                OrderDetail.subtotal,
                OrderDetail.discount,
                OrderDetail.lbctax,
                OrderDetail.total,
                Product.id.label("product_id"),
                Product.merchant,
                Product.productname,
                Product.productdescription,
                Product.unitprice,
                Product.image,
                Product.ranking,
                Product.is_deleted
            ).filter(OrderDetail.order == orderid).all()
        return jsonify([_payment_detail(order_detail) for order_detail in order_details])
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/payment/accept", methods=["GET", "POST"])
@token_required
def accept():
    try:
        data = request.get_json()
        orderid = data.get("order", None)
        codes = data["codes"]

        order = Orders.query\
            .filter(Orders.id == orderid)\
            .first()

        o_detail = OrderDetail.query\
            .filter(OrderDetail.order == order.id)\
            .first()

        user = UserCustomer.query\
            .filter(UserCustomer.customer == order.customer)\
            .first()

        wallet = Wallet.query\
            .filter(Wallet.user == user.user)\
            .first()

        if order.payment == 7:
            if wallet.singleline_earning >= order.grand_total:
                wallet.singleline_earning = decimal.Decimal(wallet.singleline_earning) - decimal.Decimal(order.grand_total)
            else:
                app.logger.error("Insufficient balance")
                return jsonify(status="Insufficient balance"), 500

        order.status = "ACCEPTED"

        for code in codes:
            activation = Activation(
                user=user.user,
                activationcode=code,
                status="ACCEPTED",
                created=datetime.datetime.now(),
                order=order.id
            )
            db.session.add(activation)

        user = User.query.filter(User.id == user.user).first()

        if len(codes) >= 1000 and user.usertypeid < 4:
            user.usertypeid = 4
            app.logger.error("User type updated to PROVINCIAL")
        elif len(codes) >= 500 and user.usertypeid < 3:
            user.usertypeid = 3
            app.logger.error("User type updated to CITY")
        elif len(codes) >= 50 and user.usertypeid < 2:
            user.usertypeid = 2
            app.logger.error("User type updated to MOBILE")

        codeincome = 0

        if user.usertypeid == 4:
            codeincome = len(codes) * 25
        elif user.usertypeid == 3:
            codeincome = len(codes) * 15
        elif user.usertypeid == 2:
            codeincome = len(codes) * 10

        wallet = Wallet.query.filter(Wallet.user == user.id).first()
        wallet.ecomm_earning = decimal.Decimal(wallet.ecomm_earning) + decimal.Decimal(codeincome)

        db.session.add(
            TransactionEcommerce(
                amount=decimal.Decimal(codeincome),
                earningtype="CODES",
                earningfrom=user.id,
                user=user.id,
                order_detail=o_detail.id
            )
        )

        db.session.commit()
        return jsonify(status="Accepted"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/payment/decline", methods=["GET", "POST"])
@token_required
def decline():
    try:
        data = request.get_json()
        orderid = data.get("order", None)
        order = Orders.query \
            .filter(Orders.id == orderid) \
            .first()

        order.status = "DECLINED"

        activations = Activation.query \
            .filter(Activation.order == orderid).all()

        for activation in activations:
            activation.status = "DECLINED"

        db.session.commit()

        return jsonify(status="Declined"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/product/accept", methods=["GET", "POST"])
@token_required
def accept_product():
    try:
        data = request.get_json()
        productid = data["product"]

        product = Product.query.filter(Product.id == productid).first()
        product.is_available = True

        db.session.commit()
        return jsonify(status="Accepted"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/product/decline", methods=["GET", "POST"])
@token_required
def decline_product():
    try:
        data = request.get_json()
        productid = data["product"]

        product = Product.query.filter(Product.id == productid).first()
        product.is_deleted = True

        db.session.commit()
        return jsonify(status="Accepted"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/order/accept", methods=["GET", "POST"])
@token_required
def accept_order():
    try:
        data = request.get_json()
        orderid = data["order"]
        order = Orders.query.filter(Orders.id == orderid).first()
        order.status = "ACCEPTED"
        db.session.commit()

        order_details = OrderDetail.query.filter(OrderDetail.order == orderid).all()

        for order in order_details:
            order.status = "ACCEPTED"
            db.session.commit()

        return jsonify(status="Accepted"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/order/decline", methods=["GET", "POST"])
@token_required
def decline_order():
    try:
        data = request.get_json()
        orderid = data["order"]
        order = Orders.query.filter(Orders.id == orderid).first()
        order.status = "DECLINED"
        db.session.commit()
        return jsonify(status="Declined"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/quota/accept", methods=["GET", "POST"])
@token_required
def accept_quota():
    try:
        data = request.get_json()
        quotaid = data["quota"]

        tx = TransactionQuota.query.filter(TransactionQuota.id == quotaid).first()
        tx.status = "ACCEPTED"

        user = User.query.filter(User.id == tx.user).first()
        user.product_quota = decimal.Decimal(user.product_quota) + decimal.Decimal(tx.count)

        db.session.commit()

        return jsonify(status="Completed"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/quota/decline", methods=["GET", "POST"])
@token_required
def decline_quota():
    try:
        data = request.get_json()
        quotaid = data["quota"]

        tx = TransactionQuota.query.filter(TransactionQuota.id == quotaid).first()
        tx.status = "DECLINED"
        db.session.commit()

        return jsonify(status="Declined"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/withdraw/accept", methods=["GET", "POST"])
@token_required
def accept_withdraw():
    try:
        data = request.get_json()
        withdraw = data["withdraw"]

        tw = TransactionWithdraw.query.filter(TransactionWithdraw.id == withdraw).first()
        tw.status = "ACCEPTED"

        wallet = Wallet.query.filter(Wallet.id == tw.wallet).first()

        if tw.wallettype == "SINGLELINE":
            wallet.singleline_earning = decimal.Decimal(wallet.singleline_earning) - decimal.Decimal(tw.amount)
        elif tw.wallettype == "UNILEVEL":
            wallet.unilevel_earning = decimal.Decimal(wallet.unilevel_earning) - decimal.Decimal(tw.amount)
        elif tw.wallettype == "ECOMMERCE":
            wallet.unilevel_earning = decimal.Decimal(wallet.ecomm_earning) - decimal.Decimal(tw.amount)

        db.session.commit()

        return jsonify(status="Accepted"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/admin/withdraw/decline", methods=["GET", "POST"])
@token_required
def decline_withdraw():
    try:
        data = request.get_json()
        withdraw = data["withdraw"]

        tw = TransactionWithdraw.query.filter(TransactionWithdraw.id == withdraw).first()
        tw.status = "DECLINED"

        db.session.commit()

        return jsonify(status="Declined"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/api/email", methods=["GET", "POST"])
def email():
    try:
        data = request.get_json()
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "From LBC Contact Us: {}".format(data["subject"])
        msg['From'] = app.config["EMAIL_AUTO"]
        msg['To'] = app.config["EMAIL_ADMIN"]

        body = data["body"]
        body = "From: {}<br>{}".format(data["email"], body)

        mime = MIMEText(body, "html")
        msg.attach(mime)

        s = smtplib.SMTP_SSL(host="mail.loadbusinessclub.com", port=465)
        s.login(app.config["EMAIL_AUTO"], app.config["EMAIL_PASS"])
        s.sendmail(app.config["EMAIL_AUTO"],
                   app.config["EMAIL_ADMIN"],
                   msg.as_string())
        s.quit()
        app.logger.info("Message sent")
        return jsonify(status="Successful"), 200

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@admin.route("/Z2VuZXJhdGUgZGFpbHk")
def generate_daily():
    app.logger.error("DAILY REWARD!")

    try:
        users = User.query.filter(User.is_activated == 1).all()

        for user in users:
            app.logger.error("PROCESSING: {}".format(user.username))
            sql = query.singleline().format(user.id)

            colnames = ["id", "username", "created", "direct", "directactive",
                        "directinactive", "team", "teamactive",
                        "teaminactive", "level", "dailyreward",
                        "maxreward"]

            results = db.session.execute(sql)
            result = [row for row in results]
            singlelinedata = {colnames[col]: sing for col, sing in enumerate(result[0])}

            wallet = Wallet.query \
                .filter(Wallet.user == user.id) \
                .first()

            dailyreward = decimal.Decimal(singlelinedata["dailyreward"])
            maxreward = decimal.Decimal(singlelinedata["maxreward"])
            new_collect = decimal.Decimal(wallet.singleline_collect) + dailyreward
            old_collect = decimal.Decimal(wallet.singleline_collect)

            if new_collect > maxreward:
                diff = (
                        decimal.Decimal(maxreward) - decimal.Decimal(wallet.singleline_collect)
                )
                if (decimal.Decimal(diff) + decimal.Decimal(wallet.singleline_collect)) <= maxreward:
                    dailyreward = decimal.Decimal(diff)
                else:
                    dailyreward = decimal.Decimal(0)

                new_collect = decimal.Decimal(wallet.singleline_collect) + dailyreward

            new_earning = decimal.Decimal(wallet.singleline_earning) + dailyreward
            old_earning = decimal.Decimal(wallet.singleline_earning)

            if dailyreward > 0:
                app.logger.error("Updating Collect: {} -> {}".format(old_collect, new_collect))
                app.logger.error("Updating earning: {} -> {}".format(old_earning, new_earning))
                app.logger.error("Max reward: {}".format(maxreward))

                if new_earning <= maxreward:
                    wallet.singleline_collect = new_collect
                    wallet.singleline_earning = new_earning
                    msg = "Daily Reward Updated successfully"
                    status = "ACCEPTED"
                    db.session.commit()
                else:
                    new_earning = old_earning
                    msg = "Daily Reward Updated failed! Already at max reward"
                    status = "DENIED"

                db.session.add(
                    TransactionDaily(
                        user=user.id,
                        username=user.username,
                        message=msg,
                        status=status,
                        old_balance=old_earning,
                        new_balance=new_earning,
                        old_collect=old_collect,
                        new_collect=new_collect
                    )
                )
                db.session.commit()
            else:
                msg = "SKIP"
            app.logger.error(msg)
            app.logger.error("Done")

        return jsonify(status="Success")

    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500