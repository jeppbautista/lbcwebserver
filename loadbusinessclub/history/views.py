from flask import Blueprint, jsonify, render_template, request, g
from sqlalchemy.exc import IntegrityError

import datetime
import decimal
import os
import traceback

from loadbusinessclub import app, db, query
from loadbusinessclub.auth.views import token_required
from loadbusinessclub.helpers import generate_activation
from loadbusinessclub.models import *
from loadbusinessclub.shop.forms import Purchase

history = Blueprint("history", __name__)


@history.route("/api/history/singleline", methods=["GET", "POST"])
@token_required
def history_singleline():
    try:
        userid = g.user.id
        txdaily = TransactionDaily.query\
            .filter(TransactionDaily.user == userid)\
            .order_by(TransactionDaily.created.desc())\
            .all()

        return jsonify([tx.tojson() for tx in txdaily]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@history.route("/api/history/unilevel", methods=["GET", "POST"])
@token_required
def history_unilevel():
    try:
        userid = g.user.id
        txunilevel = TransactionUnilevel.query\
            .filter(TransactionUnilevel.userto == userid)\
            .order_by(TransactionUnilevel.created.desc())\
            .all()

        return jsonify([tx.tojson() for tx in txunilevel]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500


@history.route("/api/history/ecommerce", methods=["GET", "POST"])
@token_required
def history_ecommerce():
    try:
        userid = g.user.id
        txecomm = TransactionEcommerce.query\
            .filter(TransactionEcommerce.user == userid)\
            .order_by(TransactionEcommerce.created.desc())\
            .all()
        return jsonify([tx.tojson() for tx in txecomm]), 200
    except Exception as e:
        app.logger.error(traceback.format_exc())
        return jsonify(status="Something went wrong", message=str(e)), 500

